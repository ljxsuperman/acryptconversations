package com.toec.acryptim.dao;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.toec.acryptim.environment.ACryptIMApplication;
import com.toec.acryptim.environment.DBConstants;
import com.toec.acryptim.utils.CommonUtil;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by liujianxu on 11/3/15.
 */
public final class DBHelper extends SQLiteOpenHelper {

    protected SQLiteDatabase mDatabase;

    private AtomicInteger atomicInteger = new AtomicInteger();

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(ACryptIMApplication.getInstance(), DBConstants.DB_NAME, factory, CommonUtil.getDatabaseVersion(context));
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(ACryptIMApplication.getInstance(), DBConstants.DB_NAME, factory, CommonUtil.getDatabaseVersion(context), errorHandler);
    }

    private DBHelper(){
        super(ACryptIMApplication.getInstance(), DBConstants.DB_NAME, null, CommonUtil.getDatabaseVersion(ACryptIMApplication.getInstance()));
    }

    public synchronized SQLiteDatabase openDatabase(){
        if(atomicInteger.incrementAndGet() == 1){
            mDatabase = getWritableDatabase();
        }
        return mDatabase;
    }

    public synchronized void closeDatabase(){
        if(atomicInteger.decrementAndGet() == 0){
            mDatabase.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static class DBHelperHolder{
        private static DBHelper instance = new DBHelper();
    }

    public synchronized static DBHelper getInstance(){
        return DBHelperHolder.instance;
    }

}
