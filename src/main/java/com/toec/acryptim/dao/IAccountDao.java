package com.toec.acryptim.dao;

import com.toec.acryptim.entity.Account;

/**
 * Created by liujianxu on 11/4/15.
 */
public interface IAccountDao extends IGenericDao<Account, String> {

    void prepareAccountData(String accountNumber);

    void clearAccountData(String accountNumber);

    Account findByNumber(String accountNumber);

}
