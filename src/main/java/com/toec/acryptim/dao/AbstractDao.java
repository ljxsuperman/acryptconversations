package com.toec.acryptim.dao;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by liujianxu on 11/4/15.
 */
public abstract class AbstractDao {

    protected DBHelper dbHelper = DBHelper.getInstance();

    protected SQLiteDatabase db = dbHelper.mDatabase;

    /**
     * 开启一个事务
     */
    protected void beginTransaction(){
        db = dbHelper.openDatabase();
        dbHelper.mDatabase.beginTransaction();
    }

    /**
     * 任务完成以后对事务进行标记,表明事务成功
     */
    protected void setTransactionSuccessful(){
        db.setTransactionSuccessful();
    }

    /**
     * 提交事务,如果检测到了事务成功,则改写,否则回滚
     */
    protected void commit(){
        db.endTransaction();
        dbHelper.closeDatabase();
    }

}
