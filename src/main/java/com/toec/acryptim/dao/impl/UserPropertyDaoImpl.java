package com.toec.acryptim.dao.impl;

import android.database.Cursor;

import com.toec.acryptim.dao.AbstractDao;
import com.toec.acryptim.dao.IUserPropertyDao;
import com.toec.acryptim.entity.UserProperty;
import com.toec.acryptim.environment.ACryptIMApplication;
import com.toec.acryptim.environment.DBConstants;
import com.toec.acryptim.utils.CommonUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by liujianxu on 11/5/15.
 */
public class UserPropertyDaoImpl extends AbstractDao implements IUserPropertyDao {
    @Override
    public Collection<UserProperty> getAllPropertyByAccountNumber(String accountNumber) {
        List<UserProperty> userProperties = new ArrayList<>();
        List<String> parentIdList = new ArrayList<>();
        try{
            beginTransaction();
            String sql = "select * from " + DBConstants.T_USER_PROPERTIES(ACryptIMApplication.getInstance().getAccountNumber())
                    + " where col_number=?;";
            Cursor c = db.rawQuery(sql, new String[]{accountNumber});
            if(c.moveToFirst()){
                do{
                    UserProperty userProperty = new UserProperty(
                            c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_ID),
                            c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_PROP_KEY),
                            UserProperty.Type.fromInt(c.getInt(DBConstants.INDEX_T_USER_PROPERTIES_COL_PROP_VALUE_TYPE)),
                            c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_PROP_VALUE),
                            c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_NUMBER)
                    );
                    userProperties.add(userProperty);
                }while (c.moveToNext());
            }
            c.close();
            setTransactionSuccessful();
        }catch(Exception e){

        }finally {
            commit();
        }
        return userProperties;
    }

    @Override
    public UserProperty save(UserProperty userProperty) {
        try{
            beginTransaction();
                String sql = "insert or ignore into " + DBConstants.T_USER_PROPERTIES(ACryptIMApplication.getInstance().getAccountNumber()) +
                        " (" +
                        " id, " +
                        " col_prop_key, " +
                        " col_prop_value_type, " +
                        " col_prop_value, " +
                        " col_number " +
                        ")" +
                        " values " +
                        "(?,?,?,?,?)";
                if (userProperty.getId() == null || userProperty.getId().equals("")) {
                    userProperty.setId(UUID.randomUUID().toString());
                }
                db.execSQL(sql, new Object[]{
                        userProperty.getId(),
                        userProperty.getKey(),
                        userProperty.getType().getValue(),
                        userProperty.getValue(),
                        userProperty.getNumber(),
                });
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            commit();
        }
        return null;
    }

    @Override
    public void remove(String... ids) {
        if(ids == null || ids.length == 0){
            return;
        }
        try{
            beginTransaction();
            String sql = "delete from " + DBConstants.T_USER_PROPERTIES(ACryptIMApplication.getInstance().getAccountNumber()) +
                    " where id in " +
                    "(" +
                    CommonUtil.listArray(ids[0].getClass(), ids) +
                    ");";
            db.execSQL(sql);
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            commit();
        }
    }

    @Override
    public void removeAll() {

    }

    @Override
    public void update(UserProperty userProperty) {
        if(userProperty == null || userProperty.getId() == null || userProperty.getId().equals("")){
            return;
        }
        try{
            beginTransaction();
            String sql = "update " + DBConstants.T_USER_PROPERTIES(ACryptIMApplication.getInstance().getAccountNumber()) +
                    " set" +
                    " col_prop_key = ?, " +
                    " col_prop_value_type = ?, " +
                    " col_prop_value = ?, " +
                    " col_number = ? " +
                    " where id = ?";
            db.execSQL(sql, new Object[]{
                    userProperty.getKey(),
                    userProperty.getType().getValue(),
                    userProperty.getValue(),
                    userProperty.getNumber(),
                    userProperty.getId()
            });
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            commit();
        }
    }

    @Override
    public UserProperty findById(String id) {
        UserProperty userProperty = null;
        String parentId = "";
        try{
            beginTransaction();
            String sql = "select * from " + DBConstants.T_USER_PROPERTIES(ACryptIMApplication.getInstance().getAccountNumber())
                    + " where col_id=?;";
            Cursor c = db.rawQuery(sql, new String[]{id});
            if(c.moveToFirst()){
                userProperty = new UserProperty(
                        c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_ID),
                        c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_PROP_KEY),
                        UserProperty.Type.fromInt(c.getInt(DBConstants.INDEX_T_USER_PROPERTIES_COL_PROP_VALUE_TYPE)),
                        c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_PROP_VALUE),
                        c.getString(DBConstants.INDEX_T_USER_PROPERTIES_COL_NUMBER)
                );
            }
            c.close();
            setTransactionSuccessful();
        }catch(Exception e){

        }finally {
            commit();
        }
        return userProperty;
    }

    @Override
    public Collection<UserProperty> findAll() {
        return null;
    }

    @Override
    public Collection<UserProperty> getPageData(Integer startPosition, Integer pageSize) {
        return null;
    }

    @Override
    public Integer getCount() {
        return null;
    }

    private static class SingletonHolder{
        private static final UserPropertyDaoImpl instance = new UserPropertyDaoImpl();
    }

    public static UserPropertyDaoImpl getInstance(){
        return SingletonHolder.instance;
    }

}
