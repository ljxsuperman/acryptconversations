package com.toec.acryptim.dao.impl;

import android.database.Cursor;

import com.toec.acryptim.dao.AbstractDao;
import com.toec.acryptim.dao.IAccountDao;
import com.toec.acryptim.entity.Account;
import com.toec.acryptim.environment.DBConstants;
import com.toec.acryptim.utils.CommonUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by liujianxu on 11/4/15.
 */
public class AccountDaoImpl extends AbstractDao implements IAccountDao {
    @Override
    public void prepareAccountData(String accountNumber) {
        try{
            beginTransaction();
            db.execSQL(DBConstants.SQL_CREATE_T_ACCOUNT);
            db.execSQL(DBConstants.SQL_CREATE_T_CONTACT_GROUP(accountNumber));
            db.execSQL(DBConstants.SQL_CREATE_T_CONTACT(accountNumber));
            db.execSQL(DBConstants.SQL_CREATE_USER_PROPERTIES(accountNumber));
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            commit();
        }
    }

    @Override
    public void clearAccountData(String accountNumber) {

    }

    @Override
    public Account save(Account account) {
        try{
            beginTransaction();
            String sql = "insert or ignore into " + DBConstants.T_ACCOUNT +
                    " (" +
                    " id, " +
                    " col_number, " +
                    " col_login_count, " +
                    " col_last_login_timestamp, " +
                    " col_expire_timestamp, " +
                    " col_lefttime_in_milsec " +
                    ")" +
                    " values " +
                    "(?,?,?,?,?,?)";
            if(account.getId() == null || account.getId().equals("")){
                account.setId(UUID.randomUUID().toString());
            }
            db.execSQL(sql, new Object[]{
                    account.getId(),
                    account.getNumber(),
                    account.getLoginCount(),
                    account.getLastLoginTimestamp(),
                    account.getExpireTimestamp(),
                    account.getLeftTimeInMiSec()
            });
            setTransactionSuccessful();
        }catch (Exception e){

        }finally{
            commit();
        }
        return account;
    }

    @Override
    public void remove(String... ids) {
        if(ids == null || ids.length == 0){
            return;
        }
        try{
            beginTransaction();
            String sql = "delete from " + DBConstants.T_ACCOUNT +
                    " where id in " +
                    "(" +
                    CommonUtil.listArray(ids[0].getClass(), ids) +
                    ");";
            db.execSQL(sql);
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            commit();
        }
    }

    @Override
    public void removeAll() {

    }

    @Override
    public void update(Account account) {
        if(account == null || account.getId() == null || account.getId().equals("")){
            return;
        }
        try{
            beginTransaction();
            String sql = "update " + DBConstants.T_ACCOUNT +
                    " set" +
                    " col_number = ?, " +
                    " col_login_count = ?, " +
                    " col_last_login_timestamp = ?, " +
                    " col_expire_timestamp = ?, " +
                    " col_lefttime_in_milsec = ? " +
                    " where id = ?;";
            db.execSQL(sql, new Object[]{
                    account.getId(),
                    account.getNumber(),
                    account.getLoginCount(),
                    account.getLastLoginTimestamp(),
                    account.getExpireTimestamp(),
                    account.getLeftTimeInMiSec()
            });
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            commit();
        }
    }

    @Override
    public Account findById(String id) {
        if(id == null || id.equals("")){
            return null;
        }
        Account account = null;
        try{
            beginTransaction();
            String sql = "select * from " + DBConstants.T_ACCOUNT
                    + " where id = ?;";
            Cursor c = db.rawQuery(sql, new String[]{id});
            if(c.moveToFirst()){
                account = new Account(
                        c.getString(DBConstants.INDEX_T_ACCOUNT_COL_ID),
                        c.getString(DBConstants.INDEX_T_ACCOUNT_COL_NUMBER),
                        c.getInt(DBConstants.INDEX_T_ACCOUNT_COL_LOGIN_COUNT),
                        c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_LAST_LOGIN_TIMESTAMP),
                        c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_EXPIRE_TIMESTAMP),
                        c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_LEFTTIME_IN_MILSEC)
                );
            }
            c.close();
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            commit();
        }
        return account;
    }

    @Override
    public Collection<Account> findAll() {
        List<Account> accountList = new ArrayList<>();
        try{
            beginTransaction();
            String sql = "select * from " + DBConstants.T_ACCOUNT
                    + " order by col_last_login_timestamp desc;";
            Cursor c = db.rawQuery(sql, null);
            if(c.moveToFirst()){
                do{
                    Account account = new Account(
                            c.getString(DBConstants.INDEX_T_ACCOUNT_COL_ID),
                            c.getString(DBConstants.INDEX_T_ACCOUNT_COL_NUMBER),
                            c.getInt(DBConstants.INDEX_T_ACCOUNT_COL_LOGIN_COUNT),
                            c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_LAST_LOGIN_TIMESTAMP),
                            c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_EXPIRE_TIMESTAMP),
                            c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_LEFTTIME_IN_MILSEC)
                    );
                    accountList.add(account);
                }while (c.moveToNext());
            }
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            commit();
        }
        return accountList;
    }

    @Override
    public Collection<Account> getPageData(Integer startPosition, Integer pageSize) {
        return null;
    }

    @Override
    public Integer getCount() {
        return null;
    }

    @Override
    public Account findByNumber(String accountNumber) {
        if(accountNumber == null || accountNumber.equals("")){
            return null;
        }
        Account account = null;
        try{
            beginTransaction();
            String sql = "select * from " + DBConstants.T_ACCOUNT
                    + " where col_number = ?;";
            Cursor c = db.rawQuery(sql, new String[]{accountNumber});
            if(c.moveToFirst()){
                account = new Account(
                        c.getString(DBConstants.INDEX_T_ACCOUNT_COL_ID),
                        c.getString(DBConstants.INDEX_T_ACCOUNT_COL_NUMBER),
                        c.getInt(DBConstants.INDEX_T_ACCOUNT_COL_LOGIN_COUNT),
                        c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_LAST_LOGIN_TIMESTAMP),
                        c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_EXPIRE_TIMESTAMP),
                        c.getLong(DBConstants.INDEX_T_ACCOUNT_COL_LEFTTIME_IN_MILSEC)
                );
            }
            c.close();
            setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            commit();
        }
        return account;
    }

    private static class SingletonHolder{
        private static final AccountDaoImpl instance = new AccountDaoImpl();
    }

    public static AccountDaoImpl getInstance(){
        return SingletonHolder.instance;
    }

}
