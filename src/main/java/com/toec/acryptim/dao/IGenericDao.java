package com.toec.acryptim.dao;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by liujianxu on 11/4/15.
 */
public interface IGenericDao<T, PK extends Serializable> {

    T save(final T entity);

    void remove(final PK... ids);

    void removeAll();

    void update(final T entity);

    T findById(final PK id);

    Collection<T> findAll();

    Collection<T> getPageData(Integer startPosition, Integer pageSize);

    Integer getCount();

}
