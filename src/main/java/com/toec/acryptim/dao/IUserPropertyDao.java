package com.toec.acryptim.dao;

import com.toec.acryptim.entity.UserProperty;

import java.util.Collection;

/**
 * Created by liujianxu on 11/5/15.
 */
public interface IUserPropertyDao extends IGenericDao<UserProperty, String> {

    public Collection<UserProperty> getAllPropertyByAccountNumber(String accountNumber);

}
