package com.toec.acryptim.ui;

import android.app.DialogFragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;

import com.toec.acryptim.R;
import com.toec.acryptim.utils.SharedPrefsUtil;


/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class LoginTipDialogFragment extends DialogFragment {

    private CheckBox skipNextTimeCb;
    private Button okBtn, quitBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View layoutView = inflater.inflate(R.layout.fragment_dialog_login_tip, container, false);
        //复选框的初始化
        skipNextTimeCb = (CheckBox) layoutView.findViewById(R.id.login_tip_fragment_skip_next_time_cb);
        skipNextTimeCb.setChecked(true);
        //确定按钮的初始化及监听器
        okBtn = (Button) layoutView.findViewById(R.id.login_tip_fragment_ok_btn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skipNextTimeCb.isChecked()) {
                    SharedPrefsUtil.setLoginTipSkipNextTime(getActivity().getApplicationContext(), true);
                }
                ((AccountActivity) getActivity()).dismissLoginTip();
            }
        });
        //退出按钮的初始化及监听器
        quitBtn = (Button) layoutView.findViewById(R.id.login_tip_fragment_quit_btn);
        quitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AccountActivity) getActivity()).dismissLoginTip();
                ((AccountActivity) getActivity()).exit();
            }
        });
        return layoutView;
    }
}
