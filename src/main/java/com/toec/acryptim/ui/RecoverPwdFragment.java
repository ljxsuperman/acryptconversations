package com.toec.acryptim.ui;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.toec.acryptim.R;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class RecoverPwdFragment extends Fragment implements TextWatcher {

    private EditText vCodeInputEt, phoneNumInputEt, pwdInputEt;
    private Button recoverConfirmBtn, sendVCodeBtn;
    private ImageButton showPwdIb;

    private boolean isPwdShow;

    private boolean isSendVCode;
    private int vCodeSecondCount = 60;

    private UIHandler uiHandler;
    private final int REFRESH_VCODE_COUNT = 0x00;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_recover_pwd, container, false);
        vCodeInputEt = (EditText) layoutView.findViewById(R.id.recover_pwd_fragment_vcode_et);
        phoneNumInputEt = (EditText) layoutView.findViewById(R.id.recover_pwd_fragment_phone_num_et);
        pwdInputEt = (EditText) layoutView.findViewById(R.id.recover_pwd_fragment_pwd_et);
        showPwdIb = (ImageButton) layoutView.findViewById(R.id.recover_pwd_fragment_show_pwd_ib);
        recoverConfirmBtn = (Button) layoutView.findViewById(R.id.recover_pwd_fragment_ok_btn);
        sendVCodeBtn = (Button) layoutView.findViewById(R.id.recover_pwd_fragment_sendvcode_btn);
        return layoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
        setListener();
    }

    //设置控件监听器
    private void setListener() {
        pwdInputEt.addTextChangedListener(this);
        phoneNumInputEt.addTextChangedListener(this);
        showPwdIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPwdShow) {
                    pwdInputEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showPwdIb.setImageResource(R.drawable.register_hide_pwd);
                    isPwdShow = false;
                } else {
                    pwdInputEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    showPwdIb.setImageResource(R.drawable.register_show_pwd);
                    isPwdShow = true;
                }
                pwdInputEt.postInvalidate();
            }
        });
        sendVCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVCodeBtn.setEnabled(false);
                isSendVCode = true;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (isSendVCode) {
                            uiHandler.sendEmptyMessage(REFRESH_VCODE_COUNT);
                            try {
                                Thread.sleep(1000);
                                vCodeSecondCount--;
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }
        });

    }

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == REFRESH_VCODE_COUNT) {
                sendVCodeBtn.setText(vCodeSecondCount + getResources().getString(R.string.recover_pwd_vcode_second_count_btn));
                if (vCodeSecondCount <= 0) {
                    isSendVCode = false;
                    sendVCodeBtn.setText(R.string.recover_pwd_fragment_send_vcode_btn);
                    sendVCodeBtn.setEnabled(true);
                    vCodeSecondCount = 60;
                }
            }
        }
    }

    private void init() {
        vCodeInputEt.setText("");
        phoneNumInputEt.setText("");
        pwdInputEt.setText("");
        recoverConfirmBtn.setEnabled(false);
        ((AccountActivity) getActivity()).setTitle(getResources().getString(R.string.recover_pwd_fragment_title));
        uiHandler = new UIHandler();
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (phoneNumInputEt.getText().toString().length() != 11 && !isSendVCode) {
            recoverConfirmBtn.setEnabled(false);
            sendVCodeBtn.setEnabled(false);
            return;
        } else {
            sendVCodeBtn.setEnabled(true);
        }
        if (!vCodeInputEt.getText().toString().equals("") && !phoneNumInputEt.getText().toString().equals("") && !pwdInputEt.getText().toString().equals("")) {
            recoverConfirmBtn.setEnabled(true);
        } else {
            recoverConfirmBtn.setEnabled(false);
        }
    }

    /**
     * 重置视图数据
     */
    public void resetView() {
        phoneNumInputEt.setText("");
        pwdInputEt.setText("");
        vCodeInputEt.setText("");
        isSendVCode = false;
        vCodeSecondCount = 60;
    }

}
