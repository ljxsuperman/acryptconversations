package com.toec.acryptim.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.toec.acryptim.R;
import com.toec.acryptim.vo.PhotoChooseItem;

import java.util.ArrayList;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class PhotoChooseAdapter extends BaseAdapter {

    private ArrayList<PhotoChooseItem> data;
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;

    public PhotoChooseAdapter(ArrayList<PhotoChooseItem> data, Context context) {
        this.data = data;
        this.context = context;
        imageLoader = ImageLoader.getInstance();
        displayImageOptions = new DisplayImageOptions.Builder().considerExifParams(true).cacheInMemory(true).cacheOnDisk(true).showImageOnFail(R.drawable.single_chat_main_send_image_icon).build();
    }

    @Override
    public int getCount() {
        if (data == null) {
            return 0;
        } else {
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (data == null) {
            return null;
        } else {
            return data.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_photo_choose, null);
            holder = new ViewHolder();
            holder.picIv = (ImageView) convertView.findViewById(R.id.item_photo_choose_iv);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            holder.picIv.setImageResource(R.drawable.photo_choose_camera);
        } else {
            imageLoader.displayImage("file://" + data.get(position).getFilePath(), holder.picIv, displayImageOptions);
        }
        return convertView;
    }

    private class ViewHolder {
        private ImageView picIv;
    }

}
