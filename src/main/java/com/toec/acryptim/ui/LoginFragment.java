package com.toec.acryptim.ui;

import android.app.Fragment;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.toec.acryptim.R;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class LoginFragment extends Fragment implements TextWatcher {

    private EditText phoneNumInputEt, pwdInputEt;
    private Button loginConfirmBtn, forgetPwdBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_login, container, false);
        phoneNumInputEt = (EditText) layoutView.findViewById(R.id.login_fragment_phone_num_et);
        pwdInputEt = (EditText) layoutView.findViewById(R.id.login_fragment_pwd_et);
        loginConfirmBtn = (Button) layoutView.findViewById(R.id.login_fragment_confirm_btn);
        forgetPwdBtn = (Button) layoutView.findViewById(R.id.login_fragment_forget_pwd_btn);
        return layoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
        setListener();
    }


    //设置控件监听器
    private void setListener() {
        phoneNumInputEt.addTextChangedListener(this);
        pwdInputEt.addTextChangedListener(this);
        loginConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AccountActivity) getActivity()).doLoginOpr(phoneNumInputEt.getText().toString(), pwdInputEt.getText().toString());
            }
        });
        forgetPwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AccountActivity) getActivity()).jumpToRecoverPwd();
            }
        });
    }

    private void init() {
        ((AccountActivity) getActivity()).setTitle(getResources().getString(R.string.login_fragment_title));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!phoneNumInputEt.getText().toString().equals("") && !pwdInputEt.getText().toString().equals("")) {
            loginConfirmBtn.setEnabled(true);
        } else {
            loginConfirmBtn.setEnabled(false);
        }
    }

    /**
     * 重置视图数据
     */
    public void resetView() {
        phoneNumInputEt.setText("");
        pwdInputEt.setText("");
    }

}
