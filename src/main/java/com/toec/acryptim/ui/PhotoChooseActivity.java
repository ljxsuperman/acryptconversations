package com.toec.acryptim.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.toec.acryptim.R;
import com.toec.acryptim.environment.CommonSettings;
import com.toec.acryptim.ui.PhotoAdjustForAvatorFragment;
import com.toec.acryptim.ui.PhotoChooseFragment;
import com.toec.acryptim.ui.XmppActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */

public class PhotoChooseActivity extends Activity {

    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;

    private PhotoChooseFragment photoChooseFragment;
    private PhotoAdjustForAvatorFragment photoAdjustForAvatorFragment;

    private int launchMode;

    private int currentPos;

//    private ImageButton backBtn;
//    private TextView titleTv;
//    private Button confirmBtn;

    private final int REQCODE_CAMERA = 0x01;

    private File imgFile;

    private ActionBar actionBar;
    private MenuItem confirmMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_choose);
        findView();
        setListener();
        init();
    }

    //初始化控件
    private void findView() {
//        backBtn = (ImageButton) findViewById(R.id.photo_choose_activity_top_bar_back_ib);
//        titleTv = (TextView) findViewById(R.id.photo_choose_activity_top_bar_title_tv);
//        confirmBtn = (Button) findViewById(R.id.photo_choose_activity_confirm_btn);
    }

    //设置控件监听器
    private void setListener() {
//        //返回按钮
//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                backOprHandle();
//            }
//        });
//        //确定按钮
//        confirmBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    File file = new File(CommonSettings.APP_PATH);
//                    if (!file.exists() || file.isFile()) {
//                        file.mkdirs();
//                    }
//                    file = new File(CommonSettings.PHOTO_CHOOSE_AVATOR_PATH);
//                    if (file.exists() && file.isFile()) {
//                        file.delete();
//                    }
//                    file.createNewFile();
//                    FileOutputStream fileOutputStream = new FileOutputStream(file);
//                    photoAdjustForAvatorFragment.getAdjustImageBitmap().compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
//                    fileOutputStream.flush();
//                    fileOutputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                photoAdjustForAvatorFragment.resetView();
//                setResult(CommonSettings.PHOTO_CHOOSE_RESULT_OK);
//                finish();
//            }
//        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            backOprHandle();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        confirmMenuItem = menu.add(getResources().getString(R.string.common_complete));
        confirmMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (launchMode) {
            case CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE_AVATOR:
                if (currentPos == CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_ADJUST) {
                    try {
                        File file = new File(CommonSettings.APP_PATH);
                        if (!file.exists() || file.isFile()) {
                            file.mkdirs();
                        }
                        file = new File(CommonSettings.PHOTO_CHOOSE_AVATOR_PATH);
                        if (file.exists() && file.isFile()) {
                            file.delete();
                        }
                        file.createNewFile();
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        photoAdjustForAvatorFragment.getAdjustImageBitmap().compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    photoAdjustForAvatorFragment.resetView();
                    setResult(CommonSettings.PHOTO_CHOOSE_RESULT_OK);
                }
                if (currentPos == CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_CHOOSE) {
                    setResult(CommonSettings.PHOTO_CHOOSE_RESULT_QUIT);
                }
                finish();
                break;
        }

        return super.onMenuItemSelected(featureId, item);
    }

    //初始化界面数据
    private void init() {
        //设置ActionBar
        actionBar = getActionBar();
        actionBar.setTitle(R.string.photo_choose_fragment_title);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);


        launchMode = getIntent().getIntExtra(CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE, -1);
        switch (launchMode) {
            case CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE_AVATOR:
                //选择头像
                performAvatorPick();
                break;
            case CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE_PHOTO:
                //选择照片
                performPhotoPick();
                break;
        }
    }

    //照片选择器
    private void performPhotoPick() {
//        if (confirmMenuItem != null) {
//            confirmMenuItem.setVisible(false);
//        }
        photoChooseFragment = new PhotoChooseFragment();
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.photo_choose_activity_fragment_container_fl, photoChooseFragment);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
        currentPos = CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_CHOOSE;
    }

    //头像选择器
    private void performAvatorPick() {
//        if (confirmMenuItem != null) {
//            confirmMenuItem.setVisible(false);
//        }
        photoChooseFragment = new PhotoChooseFragment();
        photoAdjustForAvatorFragment = new PhotoAdjustForAvatorFragment();
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.photo_choose_activity_fragment_container_fl, photoChooseFragment);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
        currentPos = CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_CHOOSE;
    }

    /**
     * 设置标题栏文字
     *
     * @param titleStr 标题栏文字
     */
    public void setTitle(String titleStr) {
//        titleTv.setText(titleStr);
    }

    //返回键及左上角后退按钮的处理
    private void backOprHandle() {
        switch (currentPos) {
            case CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_ADJUST:
//                if (confirmMenuItem != null) {
//                    confirmMenuItem.setVisible(false);
//                }
                photoAdjustForAvatorFragment.resetView();
            case CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_CHOOSE:
            default:
                setResult(CommonSettings.PHOTO_CHOOSE_RESULT_QUIT);
                finish();
                ImageLoader.getInstance().clearMemoryCache();
                ImageLoader.getInstance().clearDiskCache();
                break;
        }
    }

    /**
     * 跳转回图片
     *
     * @param photoPathStr 图片路径
     */
    public void backToPhotoChooseActivity(String photoPathStr) {
        switch (launchMode) {
            case CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE_AVATOR:
//                if (confirmMenuItem != null) {
//                    confirmMenuItem.setVisible(true);
//                }
                photoAdjustForAvatorFragment.setPhotoPath(photoPathStr);
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.photo_choose_activity_fragment_container_fl, photoAdjustForAvatorFragment);
                fragmentTransaction.commit();
                fragmentManager.executePendingTransactions();
                currentPos = CommonSettings.PHOTO_CHOOSE_ACTIVITY_POS_ADJUST;
                break;
            case CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE_PHOTO:
//                if (confirmMenuItem != null) {
//                    confirmMenuItem.setVisible(true);
//                }
                Intent intent = new Intent();
                intent.putExtra(CommonSettings.PHOTO_PATH, photoPathStr);
                setResult(CommonSettings.PHOTO_CHOOSE_RESULT_OK, intent);
                finish();
                break;
        }
    }

    /**
     * 从摄像头拍照
     */
    public void launchCamera() {
        try {
            imgFile = new File(CommonSettings.PHOTO_PATH);
            if (!imgFile.exists() || imgFile.isFile()) {
                imgFile.mkdirs();
            }
            imgFile = new File(CommonSettings.PHOTO_CHOOSE_CAMERA_CACHE_PATH + System.currentTimeMillis() + ".jpg");
            if (imgFile.exists() || imgFile.isFile()) {
                imgFile.delete();
            }
            imgFile.createNewFile();
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imgFile));
            startActivityForResult(cameraIntent, REQCODE_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQCODE_CAMERA) {
            try {
                if (resultCode == RESULT_OK) {
                    photoChooseFragment.resetView();
                    backToPhotoChooseActivity(imgFile.getAbsolutePath());
                    MediaScannerConnection.scanFile(this, new String[]{imgFile.getPath()}, null, null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
