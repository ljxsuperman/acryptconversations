package com.toec.acryptim.ui;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.toec.acryptim.R;
import com.toec.acryptim.biz.IAccountBiz;
import com.toec.acryptim.biz.IAccountNetBiz;
import com.toec.acryptim.biz.impl.AccountBizImpl;
import com.toec.acryptim.biz.impl.AccountNetBizImpl;
import com.toec.acryptim.dto.UserInfo;
import com.toec.acryptim.environment.CommonSettings;
import com.toec.acryptim.net.HttpConnetorCallback;
import com.toec.acryptim.net.TokenManager;
import com.toec.acryptim.utils.ByteUtil;
import com.toec.acryptim.utils.SharedPrefsUtil;
import com.toec.acryptim.utils.SimpleUIUtil;


/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class AccountActivity extends XmppActivity {

    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;

    private LoginTipDialogFragment loginTipDialogFragment;
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;
    private PhotoChooseFragment photoChooseFragment;
    private RecoverPwdFragment recoverPwdFragment;
    private FrameLayout fragmentContainerFl;

    private String pwdSha1;

//    private TextView titleTv;
//    private ImageButton backBtn;
//    private RelativeLayout topBarRl;

    private RelativeLayout bottomOprBtnRl;
    private Button loginBtn, regBtn;

    private int currentPos;

    private final int REQUEST_CODE_CHOOSE_AVATOR = 0x00;

    public static Handler mHandler;
    public static AccountActivity instance;

    private IAccountNetBiz accountNetBiz;
    private IAccountBiz accountBiz;

    @Override
    protected void refreshUiReal() {

    }

    @Override
    void onBackendConnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_account);
        findView();
        setListener();
        //初始化所有与账户相关的Fragment。
        initFragment();
        dismissTopBar();
        //登陆前需要显示登录提示框
        if (!SharedPrefsUtil.getLoginTipSkipNextTime(this)) {
            showLoginTip();
        }
        mHandler = new Handler();
        accountNetBiz = AccountNetBizImpl.getInstance();
        accountBiz = AccountBizImpl.getInstance();
    }

    //初始化控件
    private void findView() {
        loginBtn = (Button) findViewById(R.id.account_activity_login_btn);
        regBtn = (Button) findViewById(R.id.account_activity_register_btn);
        bottomOprBtnRl = (RelativeLayout) findViewById(R.id.account_activity_bottom_button_rl);
        fragmentContainerFl = (FrameLayout) findViewById(R.id.account_activity_fragment_container_fl);
    }

    //设置控件监听器
    private void setListener() {
        //返回按钮
//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                backOprHandle();
//            }
//        });
        //登陆按钮
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToLogin();
            }
        });
        //注册按钮
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToReg();
            }
        });
    }

    private void initFragment() {
        currentPos = 0x00;
        //登录提示框
        loginTipDialogFragment = new LoginTipDialogFragment();
        //注册界面
        registerFragment = new RegisterFragment();
        //登录界面
        loginFragment = new LoginFragment();
        //照片选择界面
        photoChooseFragment = new PhotoChooseFragment();
        //重置面膜界面
        recoverPwdFragment = new RecoverPwdFragment();
    }

    //返回键及左上角后退按钮的处理
    private void backOprHandle() {
        switch (currentPos) {
            case CommonSettings.ACCOUNT_ACTIVITY_POS_REG:
            case CommonSettings.ACCOUNT_ACTIVITY_POS_LOGIN:
                dismissTopBar();
                resetView();
                break;
            case CommonSettings.ACCOUNT_ACTIVITY_POS_RECOVER_PWD:
                recoverPwdFragment.resetView();
                jumpToLogin();
                break;
            default:
                finish();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            backOprHandle();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //重置为默认界面
    private void resetView() {
        bottomOprBtnRl.setVisibility(View.VISIBLE);
        fragmentContainerFl.setVisibility(View.GONE);
        switch (currentPos) {
            case CommonSettings.ACCOUNT_ACTIVITY_POS_LOGIN:
                loginFragment.resetView();
                break;
            case CommonSettings.ACCOUNT_ACTIVITY_POS_REG:
                registerFragment.resetView();
                break;
        }
        currentPos = 0x00;
    }

    /**
     * 退出
     */
    public void exit() {
        finish();
    }

    /**
     * 跳转到登录界面
     */
    public void jumpToLogin() {
        showTopBar();
        fragmentContainerFl.setVisibility(View.VISIBLE);
        bottomOprBtnRl.setVisibility(View.GONE);
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.account_activity_fragment_container_fl, loginFragment);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
        currentPos = CommonSettings.ACCOUNT_ACTIVITY_POS_LOGIN;
    }

    /**
     * 跳转到注册界面
     */
    public void jumpToReg() {
        showTopBar();
        fragmentContainerFl.setVisibility(View.VISIBLE);
        bottomOprBtnRl.setVisibility(View.GONE);
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.account_activity_fragment_container_fl, registerFragment);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
        currentPos = CommonSettings.ACCOUNT_ACTIVITY_POS_REG;
    }

    /**
     * 跳转到照片选择界面
     */
    public void jumpToPhotoChoose() {
        Intent photoChooseIntent = new Intent(instance, PhotoChooseActivity.class);
        photoChooseIntent.putExtra(CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE, CommonSettings.LAUNCH_MODE_PHOTO_CHOOSE_AVATOR);
        startActivityForResult(photoChooseIntent, REQUEST_CODE_CHOOSE_AVATOR);
    }

    /**
     * 跳转到密码找回界面
     */
    public void jumpToRecoverPwd() {
        showTopBar();
        fragmentContainerFl.setVisibility(View.VISIBLE);
        bottomOprBtnRl.setVisibility(View.GONE);
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.account_activity_fragment_container_fl, recoverPwdFragment);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
        currentPos = CommonSettings.ACCOUNT_ACTIVITY_POS_RECOVER_PWD;
    }

    /**
     * 跳转到主功能界面
     */
    public void jumpToMain() {
        finish();
    }

    /**
     * 显示登陆提示框
     */
    public void showLoginTip() {
        fragmentManager = getFragmentManager();
        loginTipDialogFragment.setCancelable(false);
        loginTipDialogFragment.show(fragmentManager, "login_tip_dialog");
    }

    /**
     * 消失登陆提示框
     */
    public void dismissLoginTip() {
        loginTipDialogFragment.dismiss();
    }

    /**
     * 设置顶栏标题
     *
     * @param titleStr 标题文字
     */
    public void setTitle(String titleStr) {
//        titleTv.setText(titleStr);
    }

    /**
     * 隐藏顶栏
     */
    public void dismissTopBar() {
//        topBarRl.setVisibility(View.GONE);
    }

    /**
     * 显示顶栏
     */
    public void showTopBar() {
//        topBarRl.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE_AVATOR) {
            //照片选取
            if (resultCode == CommonSettings.PHOTO_CHOOSE_RESULT_OK) {
                registerFragment.setAvatorImage(CommonSettings.PHOTO_CHOOSE_AVATOR_PATH);
            } else {
                registerFragment.resetAvatorImage();
            }
        }
    }

    /**
     * 登录操作
     * 1. kmc登陆
     * 2. xmpp服务器登陆
     * 3. 从xmpp服务器上获取自己的VCard,然后断开xmpp连接
     * 4. 保存用户信息并且进入软件
     * 本方法是登陆步骤1: kmc登陆
     *
     * @param phoneNum
     * @param pwd
     */
    public void doLoginOpr(final String phoneNum, String pwd) {
        pwdSha1 = ByteUtil.sha1(pwd);
        accountNetBiz.loginAccount(phoneNum, pwdSha1, new HttpConnetorCallback() {
            @Override
            public void onStart() {
                SimpleUIUtil.showCommonProgressDialog(instance);
            }

            @Override
            public void onCancel() {
                SimpleUIUtil.dismissProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, int errcode, String errmsg, final Bundle data) {
                if (errcode == 0) {
                    doXmppLoginOpr(data);
                } else {
                    SimpleUIUtil.dismissProgressDialog();
                    SimpleUIUtil.showShortToast(instance, errmsg);
                }
            }

            @Override
            public void onRespInvalid(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_json_err);
            }

            @Override
            public void onFailure(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_no_net);
            }
        });
    }

    /**
     * 登陆步骤2: xmpp 服务器登陆
     */
    private void doXmppLoginOpr(final Bundle data) {
        final UserInfo userInfo = ((UserInfo) data.getSerializable("userinfo"));
    }

    /**
     * 登陆步骤3: 从xmpp服务器上获取自己的VCard,然后断开xmpp连接
     *
     * @param userInfo
     * @param data
     */
    private void doXmppLoginGetMyVcardOpr(UserInfo userInfo, Bundle data) {

    }

    /**
     * 登陆步骤4: 保存用户信息并且进入软件
     *
     * @param userInfo
     * @param data
     */
    private void doSaveAccountInfoOpr(UserInfo userInfo, Bundle data) {
        jumpToMain();
    }

    /**
     * 注册时候发送短信验证码
     *
     * @param number
     */
    public void doRegUserSendVcodeOpr(String number) {
        accountNetBiz.regUserVcodeSend(number, new HttpConnetorCallback() {
            @Override
            public void onStart() {
                SimpleUIUtil.showCommonProgressDialog(instance);
            }

            @Override
            public void onCancel() {
                SimpleUIUtil.dismissProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, int errcode, String errmsg, Bundle data) {
                SimpleUIUtil.dismissProgressDialog();
                if (errcode == 0) {
                    registerFragment.startVcodeTick();
                    SimpleUIUtil.showShortToast(instance, R.string.account_activity_send_vcode_hint_success_toast);
                    TokenManager.getInstance().saveTmpToken(data.getString("token"));
                } else {
                    SimpleUIUtil.showShortToast(instance, errmsg);
                }
            }

            @Override
            public void onRespInvalid(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_json_err);
            }

            @Override
            public void onFailure(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_no_net);
            }
        });
    }

    /**
     * 注册操作
     * 1. 验证短信验证码
     * 2. kmc注册用户
     * 3. xmpp注册用户
     * 4. xmpp登陆用户
     * 5. xmpp获取vcard
     * 6. 补足vcard信息,断开xmpp连接,并且跳转到登陆页面
     * 本方法是注册步骤1:验证短信验证码
     *
     * @param number
     * @param password
     * @param nickname
     * @param vcode
     * @param avatar
     */
    public void doCheckRegVcodeAndRegUserOpr(final String number, final String password, final String nickname, String vcode, final byte[] avatar) {
        accountNetBiz.regUserVcodeCheck(TokenManager.getInstance().getTmpToken(), vcode, new HttpConnetorCallback() {
            @Override
            public void onStart() {
                SimpleUIUtil.showCommonProgressDialog(instance);
            }

            @Override
            public void onCancel() {
                SimpleUIUtil.dismissProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, int errcode, String errmsg, Bundle data) {
                SimpleUIUtil.dismissProgressDialog();
                if (errcode == 0) {
                    doRegToKmcOpr(number, password, nickname, avatar);
                } else {
                    SimpleUIUtil.dismissProgressDialog();
                    SimpleUIUtil.showShortToast(instance, errmsg);
                }
            }

            @Override
            public void onRespInvalid(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_json_err);
            }

            @Override
            public void onFailure(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_no_net);
            }
        });
    }

    /**
     * 注册步骤2: kmc注册用户
     *
     * @param number
     * @param password
     */
    private void doRegToKmcOpr(final String number, final String password, final String nickname, final byte[] avatar) {
        accountNetBiz.regUserRegist(TokenManager.getInstance().getTmpToken(), password, new HttpConnetorCallback() {
            @Override
            public void onStart() {
                // 已经显示dialog,不用再次显示
            }

            @Override
            public void onCancel() {
                SimpleUIUtil.dismissProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, int errcode, String errmsg, Bundle data) {
                if (errcode == 0) {
                    doRegToXmppOpr(number, password, data, nickname, avatar);
                } else {
                    SimpleUIUtil.dismissProgressDialog();
                    SimpleUIUtil.showShortToast(instance, errmsg);
                }
            }

            @Override
            public void onRespInvalid(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_json_err);
            }

            @Override
            public void onFailure(int statusCode, String respBody) {
                SimpleUIUtil.dismissProgressDialog();
                SimpleUIUtil.showShortToast(instance, R.string.common_net_no_net);
            }
        });
    }

    /**
     * 注册步骤3: xmpp注册用户
     *
     * @param number
     * @param password
     * @param data
     */
    private void doRegToXmppOpr(final String number, final String password, final Bundle data, final String nickname, final byte[] avatar) {
        final UserInfo userInfo = ((UserInfo) data.getSerializable("userinfo"));
    }

    /**
     * 注册步骤4: xmpp登陆用户
     */
    private void doLoginToXmppForRegOpr(final Bundle data, final String nickname, final byte[] avatar) {
        final UserInfo userInfo = ((UserInfo) data.getSerializable("userinfo"));
    }

    /**
     * 注册步骤5: xmpp获取vcard
     */
    private void getMyVcardForRegOpr(UserInfo userInfo, Bundle data, final String nickname, final byte[] avatar) {

    }

    /**
     * 注册步骤6: 补足vcard信息,断开xmpp连接,并且跳转到登陆页面
     */
    private void saveVcardInfoForRegOpr(UserInfo userInfo, Bundle data, final String nickname, final byte[] avatar) {
        jumpToMain();
    }

}
