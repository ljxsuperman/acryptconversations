package com.toec.acryptim.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.toec.acryptim.R;
import com.toec.acryptim.environment.CommonSettings;
import com.toec.acryptim.utils.ByteUtil;
import com.toec.acryptim.utils.FileUtils;

import java.io.IOException;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class RegisterFragment extends Fragment implements View.OnTouchListener, TextWatcher {

    private EditText nickNameInputEt, phoneNumInputEt, pwdInputEt, vCodeInputEt;
    private Button regConfirmBtn, sendVCodeBtn;
    private ImageButton showPwdIb;
    private ImageView setAvatorIv;
    private LinearLayout nickNameLl, phoneNumLl, allContentLl;
    private RelativeLayout pwdRl;
    private ScrollView allContentSv;

    private Handler handler;

    private boolean isPwdShow;

    private boolean isSendVCode;
    private int vCodeSecondCount = 60;

    private ImageLoader imageLoader;

    private UIHandler uiHandler;
    private final int REFRESH_VCODE_COUNT = 0x00;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_register, container, false);
        nickNameInputEt = (EditText) layoutView.findViewById(R.id.register_fragment_nickname_et);
        phoneNumInputEt = (EditText) layoutView.findViewById(R.id.register_fragment_phone_num_et);
        pwdInputEt = (EditText) layoutView.findViewById(R.id.register_fragment_pwd_et);
        regConfirmBtn = (Button) layoutView.findViewById(R.id.register_fragment_ok_btn);
        showPwdIb = (ImageButton) layoutView.findViewById(R.id.register_fragment_show_pwd_ib);
        setAvatorIv = (ImageView) layoutView.findViewById(R.id.register_fragment_choose_avator_iv);
        nickNameLl = (LinearLayout) layoutView.findViewById(R.id.register_fragment_nickname_ll);
        phoneNumLl = (LinearLayout) layoutView.findViewById(R.id.register_fragment_phone_num_ll);
        pwdRl = (RelativeLayout) layoutView.findViewById(R.id.register_fragment_pwd_rl);
        allContentSv = (ScrollView) layoutView.findViewById(R.id.register_fragment_all_content_sv);
        allContentLl = (LinearLayout) layoutView.findViewById(R.id.register_fragment_all_content_ll);
        sendVCodeBtn = (Button) layoutView.findViewById(R.id.register_fragment_sendvcode_btn);
        vCodeInputEt = (EditText) layoutView.findViewById(R.id.register_fragment_vcode_et);
        init();
        return layoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setListener();
    }

    //设置控件监听器
    private void setListener() {
        pwdInputEt.setOnTouchListener(this);
        allContentLl.setOnTouchListener(this);
        pwdInputEt.addTextChangedListener(this);
        phoneNumInputEt.addTextChangedListener(this);
        pwdInputEt.addTextChangedListener(this);
        showPwdIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPwdShow) {
                    pwdInputEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showPwdIb.setImageResource(R.drawable.register_hide_pwd);
                    isPwdShow = false;
                } else {
                    pwdInputEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    showPwdIb.setImageResource(R.drawable.register_show_pwd);
                    isPwdShow = true;
                }
                pwdInputEt.postInvalidate();
            }
        });
        setAvatorIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AccountActivity) getActivity()).jumpToPhotoChoose();
            }
        });
        regConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] avatar = null;
                if (FileUtils.isFileExists(CommonSettings.PHOTO_CHOOSE_AVATOR_PATH)) {
                    try {
                        avatar = FileUtils.readFileBytes(CommonSettings.PHOTO_CHOOSE_AVATOR_PATH);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                ((AccountActivity) getActivity()).doCheckRegVcodeAndRegUserOpr(
                        phoneNumInputEt.getText().toString(),
                        ByteUtil.sha1(pwdInputEt.getText().toString()),
                        nickNameInputEt.getText().toString(),
                        vCodeInputEt.getText().toString(),
                        avatar
                );
            }
        });
        sendVCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AccountActivity) getActivity()).doRegUserSendVcodeOpr(phoneNumInputEt.getText().toString());
            }
        });
    }

    private void init() {
        regConfirmBtn.setEnabled(false);
        ((AccountActivity) getActivity()).setTitle(getResources().getString(R.string.register_fragment_title));
        handler = new Handler();
        uiHandler = new UIHandler();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.register_fragment_pwd_et:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            allContentSv.fullScroll(ScrollView.FOCUS_DOWN);
                            pwdInputEt.requestFocus();
                        }
                    }, 500);
                }
                break;
            case R.id.register_fragment_all_content_ll:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
                break;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (phoneNumInputEt.getText().toString().length() != 11 && !isSendVCode) {
            regConfirmBtn.setEnabled(false);
            sendVCodeBtn.setEnabled(false);
            return;
        } else {
            sendVCodeBtn.setEnabled(true);
        }
        if (!nickNameInputEt.getText().toString().equals("") && !phoneNumInputEt.getText().toString().equals("") && !pwdInputEt.getText().toString().equals("")) {
            regConfirmBtn.setEnabled(true);
        } else {
            regConfirmBtn.setEnabled(false);
        }
    }

    /**
     * 重置视图数据
     */
    public void resetView() {
        regConfirmBtn.setEnabled(false);
        phoneNumInputEt.setText("");
        pwdInputEt.setText("");
        nickNameInputEt.setText("");
        setAvatorIv.setImageResource(R.drawable.register_default_avator);
        FileUtils.deleteFile(CommonSettings.PHOTO_CHOOSE_AVATOR_PATH);
    }

    /**
     * 设置头像图片路径
     *
     * @param imgPath
     */
    public void setAvatorImage(String imgPath) {
        if (imageLoader == null) {
            imageLoader = ImageLoader.getInstance();
        }
        imageLoader.displayImage("file://" + imgPath, setAvatorIv);
    }

    /**
     * 留空头像
     */
    public void resetAvatorImage() {
        setAvatorIv.setImageResource(R.drawable.register_default_avator);
    }

    public void startVcodeTick() {
        sendVCodeBtn.setEnabled(false);
        isSendVCode = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isSendVCode) {
                    uiHandler.sendEmptyMessage(REFRESH_VCODE_COUNT);
                    try {
                        Thread.sleep(1000);
                        vCodeSecondCount--;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == REFRESH_VCODE_COUNT) {
                sendVCodeBtn.setText(vCodeSecondCount + getResources().getString(R.string.recover_pwd_vcode_second_count_btn));
                sendVCodeBtn.setEnabled(false);
                if (vCodeSecondCount <= 0) {
                    isSendVCode = false;
                    sendVCodeBtn.setText(R.string.recover_pwd_fragment_send_vcode_btn);
                    sendVCodeBtn.setEnabled(true);
                    vCodeSecondCount = 60;
                }
            }
        }
    }

}
