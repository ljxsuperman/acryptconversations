package com.toec.acryptim.ui;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.toec.acryptim.R;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class PhotoAdjustForAvatorFragment extends Fragment {

    private String pathStr;
    private PhotoChooseForAvatorImageView photoChooseForAvatorImageView;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;

    private String photoUriStr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_photo_adjust_for_avator, container, false);
        photoChooseForAvatorImageView = (PhotoChooseForAvatorImageView) layoutView.findViewById(R.id.photo_adjust_for_avator_fragment_img);
        imageLoader = ImageLoader.getInstance();
        displayImageOptions = new DisplayImageOptions.Builder().considerExifParams(true).cacheInMemory(false).cacheOnDisk(true).build();
        photoUriStr = "file://" + pathStr;
        imageLoader.loadImage(photoUriStr, displayImageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                photoChooseForAvatorImageView.setImage(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
        return layoutView;
    }

    public void setPhotoPath(String pathStr) {
        this.pathStr = pathStr;
    }

    public void resetView() {
        photoChooseForAvatorImageView.resetBitmap();
    }

    public Bitmap getAdjustImageBitmap() {
        return photoChooseForAvatorImageView.getAdjustedBitmap(getActivity());
    }

}
