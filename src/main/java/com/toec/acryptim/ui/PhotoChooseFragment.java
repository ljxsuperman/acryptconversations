package com.toec.acryptim.ui;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.toec.acryptim.R;
import com.toec.acryptim.ui.adapter.PhotoChooseAdapter;
import com.toec.acryptim.vo.PhotoChooseItem;

import java.util.ArrayList;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class PhotoChooseFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private GridView photoShowGv;
    private PhotoChooseAdapter photoChooseAdapter;

    private final String[] STORE_IMAGES = {
            MediaStore.Images.Media.DATA
    };

    private Cursor cursor;

    private ArrayList<PhotoChooseItem> photoChooseItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_photo_choose, container, false);
        photoShowGv = (GridView) layoutView.findViewById(R.id.photo_choose_fragment_photo_list_gv);
        return layoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
        setListener();
    }

    //设置控件监听器
    private void setListener() {
        photoShowGv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    ((PhotoChooseActivity) getActivity()).launchCamera();
                } else {
                    ((PhotoChooseActivity) getActivity()).backToPhotoChooseActivity(photoChooseItems.get(position).getFilePath());
                }
            }
        });
    }

    private void init() {
        ((PhotoChooseActivity) getActivity()).setTitle(getResources().getString(R.string.photo_choose_fragment_title));
        //获取所有的缩略图信息数据
        getLoaderManager().initLoader(0, null, PhotoChooseFragment.this);
    }

    //组织数据
    private void handleAllData() {
        if (photoChooseItems != null) {
            photoChooseItems.clear();
        } else {
            photoChooseItems = new ArrayList<PhotoChooseItem>();
        }
        photoChooseItems.add(new PhotoChooseItem(R.drawable.photo_choose_camera + ""));
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    photoChooseItems.add(new PhotoChooseItem(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))));
                } while (cursor.moveToNext());
            }
        }
        if (photoChooseAdapter == null) {
            photoChooseAdapter = new PhotoChooseAdapter(photoChooseItems, getActivity().getApplicationContext());
            photoShowGv.setAdapter(photoChooseAdapter);
        } else {
            photoChooseAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 重置视图数据
     */
    public void resetView() {
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity().getApplicationContext(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI, STORE_IMAGES, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.cursor = data;
        handleAllData();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        handleAllData();
    }

}
