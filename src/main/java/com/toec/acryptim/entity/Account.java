package com.toec.acryptim.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by root on 10/28/15.
 */
public class Account implements Parcelable{

    private String id = "";

    private String number = "";

    private Map<String, UserProperty> properties = null;

    private Integer loginCount = 0;

    private Long lastLoginTimestamp = 0L;

    private Long expireTimestamp = 0L;

    private Long leftTimeInMiSec = 0L;

    public Account(){
        if(this.id == null || this.id.equals("")){
            this.id = UUID.randomUUID().toString();
        }
    }

    public Account(String id, String number, Integer loginCount, Long lastLoginTimestamp, Long expireTimestamp, Long leftTimeInMiSec) {
        this.id = id;
        this.number = number;
        this.loginCount = loginCount;
        this.lastLoginTimestamp = lastLoginTimestamp;
        this.expireTimestamp = expireTimestamp;
        this.leftTimeInMiSec = leftTimeInMiSec;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public Map<String, UserProperty> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, UserProperty> properties) {
        this.properties = properties;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Long getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp(Long lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public Long getExpireTimestamp() {
        return expireTimestamp;
    }

    public void setExpireTimestamp(Long expireTimestamp) {
        this.expireTimestamp = expireTimestamp;
    }

    public Long getLeftTimeInMiSec() {
        return leftTimeInMiSec;
    }

    public void setLeftTimeInMiSec(Long leftTimeInMiSec) {
        this.leftTimeInMiSec = leftTimeInMiSec;
    }

    public void addProperty(UserProperty userProperty){
        if(this.properties == null){
            this.properties = new HashMap<>();
        }
        this.properties.put(userProperty.getKey(), userProperty);
    }

    public void addProperty(String key, String value, UserProperty.Type type){
        UserProperty property = new UserProperty(UUID.randomUUID().toString(), key, type, value, this.getNumber());
        addProperty(property);
    }

    public UserProperty getProperty(String key){
        return properties.get(key);
    }

    protected Account(Parcel in) {
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static final String ACCOUNT_PREF_KEY_NICKNAME = "account_pref_key_nickname";

    public static final String ACCOUNT_PREF_KEY_AVATOR_PATH = "account_pref_key_avator_path";

    public static final String ACCOUNT_PREF_KEY_SEX = "account_pref_key_sex";

    public static final String ACCOUNT_PREF_KEY_BRITHDAY_LONG = "account_pref_key_brithday_long";

    public static final String ACCOUNT_PREF_KEY_AGE = "account_pref_key_age";

    public static final String ACCOUNT_PREF_KEY_AREA = "account_pref_key_area";

}
