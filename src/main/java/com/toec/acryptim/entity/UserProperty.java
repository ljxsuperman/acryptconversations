package com.toec.acryptim.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.toec.acryptim.utils.ByteUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Vector;

/**
 * Created by root on 10/28/15.
 */
public class UserProperty implements Parcelable {

    private String id = "";

    private String key = "";

    private String value = "";

    private String number = "";

    private Type type = Type.TYPE_NONE;

    protected UserProperty(Parcel in) {
    }

    public UserProperty(){

    }

    public UserProperty(String id, String key, Type type, String value, String number) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.number = number;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void put(String key, byte value){
        this.key = key;
        this.type = Type.TYPE_BYTE;
        this.value = ByteUtil.byteToHex(value);
    }

    public byte getByte(){
        if(this.type != Type.TYPE_BYTE){
            return 0x00;
        }else{
            return ByteUtil.hexToByte(value);
        }
    }

    public void put(String key, byte[] value){
        this.key = key;
        this.type = Type.TYPE_BYTE_ARRAY;
        this.value = ByteUtil.byteArrayToHexString(value);
    }

    public byte[] getByteArray(){
        if(this.type != type.TYPE_BYTE_ARRAY){
            return new byte[]{};
        }else{
            return ByteUtil.hexStringToByteArray(value);
        }
    }

    public void put(String key, char value){
        this.key = key;
        this.type = Type.TYPE_CHAR;
        this.value = String.valueOf(value);
    }

    public char getChar(){
        if(this.type != Type.TYPE_CHAR){
            return '\0';
        }else{
            return value.charAt(0);
        }
    }

    public void put(String key, char[] value){
        this.key = key;
        this.type = Type.TYPE_CHAR_ARRAY;
        this.value = String.valueOf(value);
    }

    public char[] getCharArray(){
        if(this.type != Type.TYPE_CHAR_ARRAY){
            return new char[]{};
        }else{
            return value.toCharArray();
        }
    }

    public void put(String key, short value){
        this.key = key;
        this.type = Type.TYPE_SHORT;
        this.value = String.valueOf(value);
    }

    public short getShort(){
        if(this.type != Type.TYPE_SHORT){
            return 0;
        }else{
            return Short.valueOf(value);
        }
    }

    public void put(String key, short[] value){
        this.key = key;
        this.type = Type.TYPE_SHORT;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public short[] getShortArray(){
        if(this.type != Type.TYPE_SHORT_ARRAY){
            return new short[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                short[] shortArray = new short[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    shortArray[i] = Short.valueOf(jsonArray.getString(i));
                }
                return shortArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new short[]{};
            }
        }
    }

    public void put(String key, int value){
        this.key = key;
        this.type = Type.TYPE_INT;
        this.value = String.valueOf(value);
    }

    public int getInt(){
        if(this.type != Type.TYPE_INT){
            return 0;
        }else{
            return Integer.valueOf(value);
        }
    }

    public void put(String key, int[] value){
        this.key = key;
        this.type = Type.TYPE_INT_ARRAY;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public int[] getIntArray(){
        if(this.type != Type.TYPE_INT_ARRAY){
            return new int[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                int[] intArray = new int[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    intArray[i] = Integer.valueOf(jsonArray.getString(i));
                }
                return intArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new int[]{};
            }
        }
    }

    public void put(String key, long value){
        this.key = key;
        this.type = Type.TYPE_LONG;
        this.value = String.valueOf(value);
    }

    public long getLong(){
        if(this.type != Type.TYPE_LONG){
            return 0;
        }else{
            return Long.valueOf(value);
        }
    }

    public void put(String key, long[] value){
        this.key = key;
        this.type = Type.TYPE_LONG_ARRAY;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public long[] getLongArray(){
        if(this.type != Type.TYPE_LONG_ARRAY){
            return new long[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                long[] longArray = new long[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    longArray[i] = Long.valueOf(jsonArray.getString(i));
                }
                return longArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new long[]{};
            }
        }
    }

    public void put(String key, double value){
        this.key = key;
        this.type = Type.TYPE_DOUBLE;
        this.value = String.valueOf(value);
    }

    public double getDouble(){
        if(this.type != Type.TYPE_DOUBLE){
            return 0;
        }else{
            return Double.valueOf(value);
        }
    }

    public void put(String key, double[] value){
        this.key = key;
        this.type = Type.TYPE_DOUBLE_ARRAY;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public double[] getDoubleArray(){
        if(this.type != Type.TYPE_DOUBLE_ARRAY){
            return new double[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                double[] doubleArray = new double[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    doubleArray[i] = Double.valueOf(jsonArray.getString(i));
                }
                return doubleArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new double[]{};
            }
        }
    }

    public void put(String key, float value){
        this.key = key;
        this.type = Type.TYPE_FLOAT;
        this.value = String.valueOf(value);
    }

    public float getFloat(){
        if(this.type != Type.TYPE_FLOAT){
            return 0;
        }else{
            return Float.valueOf(value);
        }
    }

    public void put(String key, float[] value){
        this.key = key;
        this.type = Type.TYPE_FLOAT_ARRAY;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public float[] getFloatArray(){
        if(this.type != Type.TYPE_FLOAT_ARRAY){
            return new float[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                float[] floatArray = new float[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    floatArray[i] = Float.valueOf(jsonArray.getString(i));
                }
                return floatArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new float[]{};
            }
        }
    }

    public void put(String key, CharSequence value){
        this.key = key;
        this.type = Type.TYPE_CHAR_SEQUENCE;
        this.value = String.valueOf(value);
    }

    public CharSequence getCharSequence(){
        if(this.type != Type.TYPE_CHAR_SEQUENCE){
            return "";
        }else{
            return String.valueOf(value);
        }
    }

    public void put(String key, CharSequence[] value){
        this.key = key;
        this.type = Type.TYPE_CHAR_SEQUENCE_ARRAY;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public CharSequence[] getCharSequenceArray(){
        if(this.type != Type.TYPE_CHAR_SEQUENCE_ARRAY){
            return new CharSequence[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                CharSequence[] charSequenceArray = new CharSequence[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    charSequenceArray[i] = String.valueOf(jsonArray.getString(i));
                }
                return charSequenceArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new CharSequence[]{};
            }
        }
    }

    public void put(String key, boolean value){
        this.key = key;
        this.type = Type.TYPE_BOOLEAN;
        this.value = String.valueOf(value);
    }

    public boolean getBoolean(){
        if(this.type != Type.TYPE_BOOLEAN){
            return false;
        }else{
            return Boolean.valueOf(value);
        }
    }

    public void put(String key, boolean[] value){
        this.key = key;
        this.type = Type.TYPE_BOOLEAN_ARRAY;
        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < value.length; ++i){
            try {
                jsonArray.put(i, String.valueOf(value[i]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.value = jsonArray.toString();
    }

    public boolean[] getBooleanArray(){
        if(this.type != Type.TYPE_BOOLEAN_ARRAY){
            return new boolean[]{};
        }else{
            try {
                JSONArray jsonArray = new JSONArray(value);
                boolean[] booleanArray = new boolean[jsonArray.length()];
                for(int i = 0; i < jsonArray.length(); ++i){
                    booleanArray[i] = Boolean.valueOf(jsonArray.getString(i));
                }
                return booleanArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return new boolean[]{};
            }
        }
    }

    public static final Creator<UserProperty> CREATOR = new Creator<UserProperty>() {
        @Override
        public UserProperty createFromParcel(Parcel in) {
            return new UserProperty(in);
        }

        @Override
        public UserProperty[] newArray(int size) {
            return new UserProperty[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static final class Type{

        private static Vector<Type> types = new Vector<Type>();

        private final int mValue;

        private final String mStringValue;

        private Type(int value, String strValue){
            mValue = value;
            mStringValue = strValue;
            types.addElement(this);
        }

        public static final Type TYPE_NONE = new Type(0, "TYPE_NONE");

        public static final Type TYPE_BYTE = new Type(1, "TYPE_BYTE");

        public static final Type TYPE_BYTE_ARRAY = new Type(2, "TYPE_BYTE_ARRAY");

        public static final Type TYPE_CHAR = new Type(3, "TYPE_CHAR");

        public static final Type TYPE_CHAR_ARRAY = new Type(4, "TYPE_CHAR_ARRAY");

        public static final Type TYPE_SHORT = new Type(5, "TYPE_SHORT");

        public static final Type TYPE_SHORT_ARRAY = new Type(6, "TYPE_SHORT_ARRAY");

        public static final Type TYPE_INT = new Type(7, "TYPE_INT");

        public static final Type TYPE_INT_ARRAY = new Type(8, "TYPE_INT_ARRAY");

        public static final Type TYPE_LONG = new Type(9, "TYPE_LONG");

        public static final Type TYPE_LONG_ARRAY = new Type(10, "TYPE_LONG_ARRAY");

        public static final Type TYPE_DOUBLE = new Type(11, "TYPE_DOUBLE");

        public static final Type TYPE_DOUBLE_ARRAY = new Type(12, "TYPE_DOUBLE_ARRAY");

        public static final Type TYPE_FLOAT = new Type(13, "TYPE_FLOAT");

        public static final Type TYPE_FLOAT_ARRAY = new Type(14, "TYPE_FLOAT_ARRAY");

        public static final Type TYPE_CHAR_SEQUENCE = new Type(15, "TYPE_CHAR_SEQUENCE");

        public static final Type TYPE_CHAR_SEQUENCE_ARRAY = new Type(16, "TYPE_CHAR_SEQUENCE_ARRAY");

        public static final Type TYPE_BOOLEAN = new Type(17, "TYPE_BOOLEAN");

        public static final Type TYPE_BOOLEAN_ARRAY = new Type(18, "TYPE_BOOLEAN_ARRAY");

        public static final Type TYPE_UNKNOWN = new Type(-1, "TYPE_UNKNOWN");

        public final int getValue(){
            return mValue;
        }

        @Override
        public final String toString(){
            return mStringValue;
        }

        public static Type fromInt(int value){
            for(Type type : types){
                if(value == type.getValue()){
                    return type;
                }
            }
            return TYPE_UNKNOWN;
        }

    }

}
