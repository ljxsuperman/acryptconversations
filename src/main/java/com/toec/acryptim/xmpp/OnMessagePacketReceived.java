package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;
import com.toec.acryptim.xmpp.stanzas.MessagePacket;

public interface OnMessagePacketReceived extends PacketReceived {
	public void onMessagePacketReceived(Account account, MessagePacket packet);
}
