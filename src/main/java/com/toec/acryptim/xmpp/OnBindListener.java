package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;

public interface OnBindListener {
	public void onBind(Account account);
}
