package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;
import com.toec.acryptim.xmpp.stanzas.IqPacket;

public interface OnIqPacketReceived extends PacketReceived {
	public void onIqPacketReceived(Account account, IqPacket packet);
}
