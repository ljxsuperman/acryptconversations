package com.toec.acryptim.xmpp.jingle;

public interface OnTransportConnected {
	public void failed();

	public void established();
}
