package com.toec.acryptim.xmpp.jingle;

import com.toec.acryptim.entities.DownloadableFile;

public interface OnFileTransmissionStatusChanged {
	void onFileTransmitted(DownloadableFile file);

	void onFileTransferAborted();
}
