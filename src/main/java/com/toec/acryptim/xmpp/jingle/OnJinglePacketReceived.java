package com.toec.acryptim.xmpp.jingle;

import com.toec.acryptim.entities.Account;
import com.toec.acryptim.xmpp.PacketReceived;
import com.toec.acryptim.xmpp.jingle.stanzas.JinglePacket;

public interface OnJinglePacketReceived extends PacketReceived {
	void onJinglePacketReceived(Account account, JinglePacket packet);
}
