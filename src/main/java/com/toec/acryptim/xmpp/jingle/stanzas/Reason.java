package com.toec.acryptim.xmpp.jingle.stanzas;

import com.toec.acryptim.xml.Element;

public class Reason extends Element {
	private Reason(String name) {
		super(name);
	}

	public Reason() {
		super("reason");
	}
}
