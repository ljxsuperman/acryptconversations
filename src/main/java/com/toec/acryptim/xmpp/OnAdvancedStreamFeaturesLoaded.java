package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;

public interface OnAdvancedStreamFeaturesLoaded {
	public void onAdvancedStreamFeaturesAvailable(final Account account);
}
