package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;

public interface OnStatusChanged {
	public void onStatusChanged(Account account);
}
