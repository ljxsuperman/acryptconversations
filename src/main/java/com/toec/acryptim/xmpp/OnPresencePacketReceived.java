package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;
import com.toec.acryptim.xmpp.stanzas.PresencePacket;

public interface OnPresencePacketReceived extends PacketReceived {
	public void onPresencePacketReceived(Account account, PresencePacket packet);
}
