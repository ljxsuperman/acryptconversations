package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Account;

public interface OnMessageAcknowledged {
	public void onMessageAcknowledged(Account account, String id);
}
