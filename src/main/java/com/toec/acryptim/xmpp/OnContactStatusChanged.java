package com.toec.acryptim.xmpp;

import com.toec.acryptim.entities.Contact;

public interface OnContactStatusChanged {
	public void onContactStatusChanged(final Contact contact, final boolean online);
}
