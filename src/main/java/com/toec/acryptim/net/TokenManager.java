package com.toec.acryptim.net;

import com.toec.acryptim.environment.ACryptIMApplication;
import com.toec.acryptim.environment.CommonSettings;
import com.toec.acryptim.utils.SharedPrefsUtil;

/**
 * Created by root on 11/16/15.
 */
public class TokenManager {

    public synchronized void saveLoginToken(String loginToken){
        SharedPrefsUtil.saveLoginToken(loginToken, ACryptIMApplication.getInstance());
    }

    public String getLoginToken(){
        return SharedPrefsUtil.getLoginToken(ACryptIMApplication.getInstance());
    }

    public synchronized void clearLoginToken(){
        SharedPrefsUtil.saveLoginToken(CommonSettings.DEFAULT_LOGIN_TOKEN, ACryptIMApplication.getInstance());
    }

    public synchronized void saveTmpToken(String tmpToken){
        SharedPrefsUtil.saveTmpToken(tmpToken, ACryptIMApplication.getInstance());
    }

    public String getTmpToken(){
        return SharedPrefsUtil.getTmpToken(ACryptIMApplication.getInstance());
    }

    public synchronized void clearTmpToken(String tmpToken){
        SharedPrefsUtil.saveTmpToken(CommonSettings.DEFAULT_TMP_TOKEN, ACryptIMApplication.getInstance());
    }

    private static class SingletonHolder{
        private static final TokenManager instance = new TokenManager();
    }

    public static TokenManager getInstance(){
        return SingletonHolder.instance;
    }
}
