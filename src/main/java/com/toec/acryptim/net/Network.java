package com.toec.acryptim.net;

/**
 * Created by liujianxu on 10/29/15.
 */
public class Network {

    public static final String XMPP_SERVER_IP = "192.168.1.251";

    public static final String KMC_SERVER_IP = "192.168.1.41";

    public static final int XMPP_SERVER_PORT = 9090;

    public static final int KMC_SERVER_HTTP_PORT = 9011;

    public static final int KMC_SERVER_HTTPS_PORT = 0;

    public static final String KMC_ADDRESS_HTTP_BASE = "http://" + KMC_SERVER_IP + ":" + KMC_SERVER_HTTP_PORT +"/jmim";

    public static final String KMC_ADDRESS_HTTPS_BASE = "https://" +  KMC_SERVER_IP + ":" + KMC_SERVER_HTTPS_PORT +"/jmim";

    public static final String HEAD_PRODUCT_NAME = "prod-name";

    public static final String HEAD_PRODUCT_VERSION = "prod-version";

    public static final String HEAD_OS_NAME = "os-name";

    public static final String HEAD_OS_VERSION = "os-version";

    public static final String HEAD_DEVICE_NAME = "device-name";

    public static final String HEAD_CONTENT_TYPE_JSON = "application/json";

    /*登陆接口*/
    public static final String KMC_LOGIN_URL = KMC_ADDRESS_HTTP_BASE + "/user/login/";

    /*登出接口*/
    public static final String KMC_LOGOUT_URL = KMC_ADDRESS_HTTP_BASE + "/user/logout";

    /*用户注册时候请求短信验证码接口*/
    public static final String KMC_REG_USER_VCODE_SEND = KMC_ADDRESS_HTTP_BASE + "/user/regist/vcode/send";

    /*用户注册时候验证短信验证码的接口*/
    public static final String KMC_REG_USER_VCODE_CHECK = KMC_ADDRESS_HTTP_BASE + "/user/regist/vcode/check";

    /*用户注册接口*/
    public static final String KMC_REG_USER_REGIST = KMC_ADDRESS_HTTP_BASE + "/user/regist";

    /*用户重置密码时候请求短信验证码接口*/
    public static final String KMC_RESET_PASSWORD_VCODE_SEND = KMC_ADDRESS_HTTP_BASE + "/user/resetpassword/vcode/send";

    /*用户重置密码时候验证短信验证码的接口*/
    public static final String KMC_RESET_PASSWORD_VCODE_CHECK = KMC_ADDRESS_HTTP_BASE + "/user/resetpassword/vcode/check";

    /*用户重置密码接口*/
    public static final String KMC_RESET_PASSWORD = KMC_ADDRESS_HTTP_BASE + "/user/resetpassword";

    /*用户充值接口*/
    public static final String KMC_RECHARGE = KMC_ADDRESS_HTTP_BASE + "/user/recharge";

    /*用户修改密码接口*/
    public static final String KMC_CHANGE_PASSWORD = KMC_ADDRESS_HTTP_BASE +"/user/changepassword";

}
