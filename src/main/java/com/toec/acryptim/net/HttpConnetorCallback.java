package com.toec.acryptim.net;

import android.os.Bundle;

/**
 * Created by liujianxu on 10/30/15.
 */
public interface HttpConnetorCallback {

    void onStart();

    void onCancel();

    void onSuccess(int statusCode, int errcode, String errmsg, Bundle data);

    void onRespInvalid(int statusCode, String respBody);

    void onFailure(int statusCode, String respBody);

}
