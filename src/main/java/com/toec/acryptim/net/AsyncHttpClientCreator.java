package com.toec.acryptim.net;

import com.loopj.android.http.AsyncHttpClient;
import com.toec.acryptim.environment.AppConstants;

/**
 * Created by liujianxu on 10/29/15.
 */
public class AsyncHttpClientCreator {

    private static AsyncHttpClient asyncHttpClient = null;

    public static AsyncHttpClient create(){
        if(asyncHttpClient == null){
            synchronized (AsyncHttpClientCreator.class){
                if(asyncHttpClient == null){
                    asyncHttpClient = new AsyncHttpClient(Network.KMC_SERVER_HTTP_PORT, Network.KMC_SERVER_HTTPS_PORT);
                    asyncHttpClient.addHeader(Network.HEAD_PRODUCT_NAME, AppConstants.PRODUCT_NAME);
                    asyncHttpClient.addHeader(Network.HEAD_PRODUCT_VERSION, AppConstants.PRODUCT_VERSION);
                    asyncHttpClient.addHeader(Network.HEAD_OS_NAME, AppConstants.OS_NAME);
                    asyncHttpClient.addHeader(Network.HEAD_OS_VERSION, AppConstants.OS_VERSION);
                    asyncHttpClient.addHeader(Network.HEAD_DEVICE_NAME, AppConstants.DEVICE_NAME);
                }
            }
        }
        return asyncHttpClient;
    }

}
