package com.toec.acryptim.environment;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.toec.acryptim.entity.Account;

/**
 * Created by liujianxu on 10/29/15.
 */
public class ACryptIMApplication extends Application {

    private static ACryptIMApplication instance = null;

    private ImageLoaderConfiguration.Builder configuration;

    private Account account = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        configuration = new ImageLoaderConfiguration.Builder(this);
        ImageLoader.getInstance().init(configuration.build());
    }

    public static ACryptIMApplication getInstance(){
        return instance;
    }

    public Account getCurrentAccount(){
        return this.account;
    }

    public void setCurrentAccount(Account account){
        this.account = account;
    }

    public String getAccountNumber() {
        if(this.account == null){
            return "";
        }
        return this.account.getNumber();
    }

}
