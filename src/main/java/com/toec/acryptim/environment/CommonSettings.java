package com.toec.acryptim.environment;

import android.os.Environment;

import java.io.File;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class CommonSettings {

    /**
     * 当前位置：注册
     */
    public static final int ACCOUNT_ACTIVITY_POS_REG = 0x01;

    /**
     * 当前位置：登录
     */
    public static final int ACCOUNT_ACTIVITY_POS_LOGIN = 0x02;

    /**
     * 当前位置：找回密码
     */
    public static final int ACCOUNT_ACTIVITY_POS_RECOVER_PWD = 0x03;

    /**
     * ContactOprActivity启动模式（必选参数）
     */
    public static final String LAUNCH_MODE_CONTACT_OPR_ACTIVITY = "launch_mode";

    /**
     * 好友搜索
     */
    public static final int LAUNCH_MODE_CONTACT_OPR_ADD_NEW_CONTACT = 0x01;

    /**
     * 查看好友资料
     */
    public static final int LAUNCH_MODE_CONTACT_OPR_VIEW_VCARD = 0x02;

    /**
     * 当前位置：添加好友
     */
    public static final int CONTACT_OPR_ACTIVITY_POS_ADD_NEW_CONTACT = 0x01;

    /**
     * 当前位置：显示详细资料
     */
    public static final int CONTACT_OPR_ACTIVITY_POS_VIEW_DETAIL = 0x02;

    /**
     * 当前位置：设置备注名称（从好友列表）
     */
    public static final int CONTACT_OPR_ACTIVITY_POS_SET_CUSTOM_NAME_FROM_CONTACT_LIST = 0x03;

    /**
     * 当前位置：设置备注名称（从详细资料）
     */
    public static final int CONTACT_OPR_ACTIVITY_POS_SET_CUSTOM_NAME_FROM_VIEW_DETAIL = 0x04;

    /**
     * PhotoChooseActivity启动模式（必选参数）
     */
    public static final String LAUNCH_MODE_PHOTO_CHOOSE = "launch_mode";

    /**
     * 头像选择器
     */
    public static final int LAUNCH_MODE_PHOTO_CHOOSE_AVATOR = 0x01;

    /**
     * 照片选择器
     */
    public static final int LAUNCH_MODE_PHOTO_CHOOSE_PHOTO = 0x02;

    /**
     * 照片路径
     */
    public static final String PHOTO_CHOOSE_RETURN_DATA_PATH = "path";

    /**
     * 当前位置：选取照片
     */
    public static final int PHOTO_CHOOSE_ACTIVITY_POS_CHOOSE = 0x01;

    /**
     * 当前位置：图像裁切
     */
    public static final int PHOTO_CHOOSE_ACTIVITY_POS_ADJUST = 0x02;

    /**
     * 头像像素
     */
    public static final int AVATOR_SIZE = 192;

    /**
     * 已选照片
     */
    public static final int PHOTO_CHOOSE_RESULT_OK = 0x01;

    /**
     * 取消选取
     */
    public static final int PHOTO_CHOOSE_RESULT_QUIT = 0x02;

    /**
     * 当前位置：我的资料
     */
    public static final int SETTING_ACTIVITY_POS_MY_PROFILE = 0x01;

    /**
     * 当前位置：通用设置
     */
    public static final int SETTING_ACTIVITY_POS_COMMON = 0x02;

    /**
     * 当前位置：通知和提醒
     */
    public static final int SETTING_ACTIVITY_POS_NOTIFICATION = 0x03;

    /**
     * 当前位置：帮助和支持
     */
    public static final int SETTING_ACTIVITY_POS_HELP = 0x04;

    /**
     * 当前位置：退出
     */
    public static final int SETTING_ACTIVITY_POS_QUIT = 0x05;

    /**
     * 当前位置：昵称设置
     */
    public static final int SETTING_ACTIVITY_POS_NICKNAME = 0x06;

    /**
     * 当前位置：聊天列表
     */
    public static final int SINGLE_CHAT_ACTIVITY_POS_MAIN = 0x01;

    /**
     * SettingActivity启动模式（必选参数）
     */
    public static final String LAUNCH_MODE_SETTING = "launch_mode";

    /**
     * 个人资料
     */
    public static final int LAUNCH_MODE_SETTING_MY_PROFILE = 0x01;

    /**
     * 通用设置
     */
    public static final int LAUNCH_MODE_SETTING_COMMON = 0x02;

    /**
     * 通知和提醒设置
     */
    public static final int LAUNCH_MODE_SETTING_NOTIFICATION = 0x03;

    /**
     * 帮助和支持设置
     */
    public static final int LAUNCH_MODE_SETTING_HELP = 0x04;

    /**
     * 退出设置
     */
    public static final int LAUNCH_MODE_SETTING_QUIT = 0x06;


    /**
     * 通用的对话框标题
     */
    public static final String COMMON_QUESTION_DIALOG_TITLE = "title";

    /**
     * 通用的对话框文字内容
     */
    public static final String COMMON_QUESTION_DIALOG_CONTENT = "content";

    /**
     * 通用的取消按钮
     */
    public static final String COMMON_QUESTION_DIALOG_NEGATIVE_BTN = "negative";

    /**
     * 通用的确定按钮
     */
    public static final String COMMON_QUESTION_DIALOG_POSITIVE_BTN = "positive";

    /**
     * 应用本地存储路径前缀
     */
    public static final String APP_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + "TOEC" + File.separator + "AcryptIM" + File.separator;

    /**
     * 图片媒体存放路径
     */
    public static final String PHOTO_PATH = APP_PATH + "cache" + File.separator + "image" + File.separator;

    /**
     * 头像本地存储路径
     */
    public static final String PHOTO_CHOOSE_AVATOR_PATH = APP_PATH + "avator.dat";

    /**
     * 拍照生成的照片路径（临时，选取照片专用）
     */
    public static final String PHOTO_CHOOSE_CAMERA_CACHE_PATH = PHOTO_PATH + "photochoose";

    /**
     * 用户头像存储路径，不能直接使用，需要添加用户帐号作为前缀命名文件。本地用户和联系人用户的头像都存放于此。
     */
    public static final String USER_AVATOR_UNFORMATTED_PATH = PHOTO_PATH + "avator" + File.separator + "%s_avator.jpg";

    /**
     * 明文语音存放路径，发送时专用，加密完成后存到缓存区并删除该明文文件
     */
    public static final String CHAT_VOICE_SEND_UNENCRYPT_MEDIA_FILE = APP_PATH + "voice.dat";

    /**
     * 登录页的提示窗口是否显示（默认显示，布尔类型）
     */
    public static final String PREF_LOGIN_TIP_SKIP_NEXT_TIME = "login_tip_skip_next_time";

    /**
     * 登录页的提示窗口是否显示默认值
     */
    public static final boolean DEFAULT_LOGIN_TIP_SKIP_NEXT_TIME = false;

    public static final String PREF_ACCOUNT_NUMBER = "pref_account_number";

    public static final String DEFAULT_ACCOUNT_NUMBER = "";

    public static final String PREF_ACCOUNT_NICKNAME = "pref_account_nickname";

    public static final String DEFALUT_ACCOUNT_NICKNAME = "";

    public static final String PREF_ACCOUNT_PASSWORD = "pref_account_password";

    public static final String DEFAULT_ACCOUNT_PASSWORD = "";

    public static final String PREF_ACCOUNT_AVATOR_PATH = "pref_account_avator_path";

    public static final String DEFAULT_ACCOUNT_AVATOR_PATH = "";

    public static final String PREF_LOGIN_TOKEN = "pref_login_token";

    public static final String DEFAULT_LOGIN_TOKEN = "";

    public static final String PREF_TMP_TOKEN = "pref_tmp_token";

    public static final String DEFAULT_TMP_TOKEN = "";

}
