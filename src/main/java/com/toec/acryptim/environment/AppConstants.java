package com.toec.acryptim.environment;

import android.os.Build;

import com.toec.acryptim.utils.CommonUtil;

/**
 * Created by liujianxu on 10/29/15.
 */
public class AppConstants {

    public static final String PRODUCT_NAME = "jmim";

    public static final String PRODUCT_VERSION = CommonUtil.getVersionName(ACryptIMApplication.getInstance());

    public static final String OS_NAME = "android";

    public static final String OS_VERSION = Build.VERSION.RELEASE;

    public static final String DEVICE_NAME = Build.MODEL;

    public static final String PACKAGE_NAME = CommonUtil.getPackageName(ACryptIMApplication.getInstance());

}
