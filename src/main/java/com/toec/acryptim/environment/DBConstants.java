package com.toec.acryptim.environment;

/**
 * Created by liujianxu on 11/3/15.
 */
public class DBConstants {

    public static final String DB_NAME = "db_acrypt_im";

    public static final String T_ACCOUNT = "t_account";

    public static final String T_CONTACT_GROUP = "t_contact_group";

    public static final String T_CONTACT = "t_contact";

    public static final String T_USER_PROPERTIES = "t_user_properties";

    public static final String T_CONTACT_GROUP(String accountNumber){
        return T_CONTACT_GROUP + "_" + accountNumber;
    }

    public static final String T_CONTACT(String accountNumber){
        return T_CONTACT + "_" + accountNumber;
    }

    public static final String T_USER_PROPERTIES(String accountNumber){
        return T_USER_PROPERTIES + "_" + accountNumber;
    }

    public static final String SQL_CREATE_T_ACCOUNT =
    "        CREATE TABLE IF NOT EXISTS                      \n" +
             T_ACCOUNT                                    + "\n" +
    "        (                                               \n" +
    "                id TEXT PRIMARY KEY,                    \n" +
    "                col_number TEXT,                        \n" +
    "                col_login_count INTEGER,                \n" +
    "                col_last_login_timestamp BIGINT,        \n" +
    "                col_expire_timestamp BIGINT,            \n" +
    "                col_lefttime_in_milsec BIGINT           \n" +
    "        );";

    public static final String SQL_CREATE_T_USER_PROPERTIES =
    "        CREATE TABLE IF NOT EXISTS                      \n" +
    "        %s                                              \n" +
    "        (                                               \n" +
    "            id TEXT PRIMARY KEY,                        \n" +
    "            col_prop_key TEXT,                          \n" +
    "            col_prop_value_type INTEGER,                \n" +
    "            col_prop_value TEXT,                        \n" +
    "            col_number TEXT                             \n" +
    "        );";

    public static final String SQL_CREATE_T_CONTACT_GROUP =
    "        CREATE TABLE IF NOT EXISTS                     \n" +
    "        %s                                             \n" +
    "        (                                              \n" +
    "            id TEXT PRIMARY KEY,                       \n" +
    "            col_group_name TEXT,                       \n" +
    "            col_group_type INTEGER,                    \n" +
    "            col_group_pinyin TEXT,                     \n" +
    "            col_group_sort TEXT,                       \n" +
    "            col_group_index TEXT,                      \n" +
    "            col_group_first_letter TEXT                \n" +
    "        );";

    public static final String SQL_CREATE_T_CONTACT =
    "        CREATE TABLE IF NOT EXISTS                     \n" +
    "        %S                                             \n" +
    "        (                                              \n" +
    "            id TEXT PRIMARY KEY,                       \n" +
    "            col_contact_number TEXT,                   \n" +
    "            col_group_id INTEGER,                      \n" +
    "            col_contact_pinyin TEXT,                   \n" +
    "            col_contact_sort TEXT,                     \n" +
    "            col_contact_index TEXT,                    \n" +
    "            col_contact_first_letter TEXT              \n" +
    "        );";

    public static final String SQL_CREATE_T_CONTACT_GROUP(String accountNumber){
        return String.format(SQL_CREATE_T_CONTACT_GROUP, T_CONTACT_GROUP(accountNumber));
    }

    public static final String SQL_CREATE_T_CONTACT(String accountNumber){
        return String.format(SQL_CREATE_T_CONTACT, T_CONTACT(accountNumber));
    }

    public static final String SQL_CREATE_USER_PROPERTIES(String accountNumber){
        return String.format(SQL_CREATE_T_USER_PROPERTIES, T_USER_PROPERTIES(accountNumber));
    }

    public static final int INDEX_T_ACCOUNT_COL_ID = 0;

    public static final int INDEX_T_ACCOUNT_COL_NUMBER = 1;

    public static final int INDEX_T_ACCOUNT_COL_LOGIN_COUNT = 2;

    public static final int INDEX_T_ACCOUNT_COL_LAST_LOGIN_TIMESTAMP = 3;

    public static final int INDEX_T_ACCOUNT_COL_EXPIRE_TIMESTAMP = 4;

    public static final int INDEX_T_ACCOUNT_COL_LEFTTIME_IN_MILSEC = 5;

    public static final int INDEX_T_USER_PROPERTIES_COL_ID = 0;

    public static final int INDEX_T_USER_PROPERTIES_COL_PROP_KEY = 1;

    public static final int INDEX_T_USER_PROPERTIES_COL_PROP_VALUE_TYPE = 2;

    public static final int INDEX_T_USER_PROPERTIES_COL_PROP_VALUE = 3;

    public static final int INDEX_T_USER_PROPERTIES_COL_NUMBER = 4;

    public static final int INDEX_T_USER_PROPERTIES_COL_LEVEL = 6;

    public static final int INDEX_T_CONTACT_COL_ID = 0;

    public static final int INDEX_T_CONTACT_COL_NUMBER = 1;

    public static final int INDEX_T_CONTACT_COL_GROUP_ID = 2;

    public static final int INDEX_T_CONTACT_COL_PINYIN = 3;

    public static final int INDEX_T_CONTACT_COL_SORT = 4;

    public static final int INDEX_T_CONTACT_COL_INDEX = 5;

    public static final int INDEX_T_CONTACT_COL_FIRST_LETTER = 6;

}
