package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 10/29/15.
 */
public class UserInfo implements Serializable, JsonFormat {

    private String username = "";
    
    private Long expiredate = 0L;
    
    private Integer validdays = 0;

    public UserInfo() {
        
    }

    public UserInfo(String username, Long expiredate, Integer validdays) {
        this.username = username;
        this.expiredate = expiredate;
        this.validdays = validdays;
    }
    
    public UserInfo(String jsonString){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.username = jsonObject.getString("username");
            this.expiredate = jsonObject.getLong("expiredate");
            this.validdays = jsonObject.getInt("validdays");
        } catch (JSONException e) {
            e.printStackTrace();
            this.username = "";
            this.expiredate = 0L;
            this.validdays = 0;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getExpiredate() {
        return expiredate;
    }

    public void setExpiredate(Long expiredate) {
        this.expiredate = expiredate;
    }

    public Integer getValiddays() {
        return validdays;
    }

    public void setValiddays(Integer validdays) {
        this.validdays = validdays;
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", this.username);
            jsonObject.put("expiredate", this.expiredate);
            jsonObject.put("validdays", this.validdays);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", this.username);
            jsonObject.put("expiredate", this.expiredate);
            jsonObject.put("validdays", this.validdays);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
