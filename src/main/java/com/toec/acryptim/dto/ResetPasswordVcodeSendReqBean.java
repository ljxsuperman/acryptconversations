package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 11/2/15.
 */
public class ResetPasswordVcodeSendReqBean implements Serializable, JsonFormat {

    private String username = "";

    public ResetPasswordVcodeSendReqBean(){

    }

    public ResetPasswordVcodeSendReqBean(String username, boolean isJsonStr){
        if(isJsonStr){
            try {
                JSONObject jsonObject = new JSONObject(username);
                this.username = jsonObject.getString("username");
            } catch (JSONException e) {
                e.printStackTrace();
                this.username = "";
            }
        }else{
            this.username = username;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", username);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", username);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
