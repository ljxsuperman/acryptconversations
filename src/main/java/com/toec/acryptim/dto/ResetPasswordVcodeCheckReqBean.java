package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 11/2/15.
 */
public class ResetPasswordVcodeCheckReqBean implements Serializable, JsonFormat{

    private String token = "";

    private String vcode = "";

    public ResetPasswordVcodeCheckReqBean(){

    }

    public ResetPasswordVcodeCheckReqBean(String token, String vcode) {
        this.token = token;
        this.vcode = vcode;
    }

    public ResetPasswordVcodeCheckReqBean(String jsonString){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.token = jsonObject.getString("token");
            this.vcode = jsonObject.getString("vcode");
        } catch (JSONException e) {
            this.token = "";
            this.vcode = "";
            e.printStackTrace();
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", this.token);
            jsonObject.put("vcode", this.vcode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", this.token);
            jsonObject.put("vcode", this.vcode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
