package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 10/29/15.
 */
public class LoginAccountReqBean implements Serializable, JsonFormat {

    private String username = "";

    private String password = "";

    public LoginAccountReqBean(){

    }

    public LoginAccountReqBean(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginAccountReqBean(String jsonString){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.username = jsonObject.getString("username");
            this.password = jsonObject.getString("password");
        } catch (JSONException e) {
            e.printStackTrace();
            this.username = "";
            this.password = "";
        }
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", this.username);
            jsonObject.put("password", this.password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", this.username);
            jsonObject.put("password", this.password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
