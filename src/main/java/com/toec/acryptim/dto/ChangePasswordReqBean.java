package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 11/2/15.
 */
public class ChangePasswordReqBean implements Serializable, JsonFormat {

    private String oldpassword = "";

    private String password = "";

    public ChangePasswordReqBean(){

    }

    public ChangePasswordReqBean(String oldpassword, String password) {
        this.oldpassword = oldpassword;
        this.password = password;
    }

    public ChangePasswordReqBean(String jsonString){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.oldpassword = jsonObject.getString("oldpassword");
            this.password = jsonObject.getString("password");
        } catch (JSONException e) {
            e.printStackTrace();
            this.oldpassword = "";
            this.password = "";
        }
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("oldpassword", this.oldpassword);
            jsonObject.put("password", this.password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("oldpassword", this.oldpassword);
            jsonObject.put("password", this.password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
