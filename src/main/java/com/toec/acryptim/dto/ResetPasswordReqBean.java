package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 11/2/15.
 */
public class ResetPasswordReqBean implements Serializable, JsonFormat{

    private String token = "";

    private String password = "";

    public ResetPasswordReqBean(){

    }

    public ResetPasswordReqBean(String token, String password) {
        this.token = token;
        this.password = password;
    }

    public ResetPasswordReqBean(String jsonString)  {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.token = jsonObject.getString("token");
            this.password = jsonObject.getString("password");
        } catch (JSONException e) {
            e.printStackTrace();
            this.token = "";
            this.password = "";
        }
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", this.token);
            jsonObject.put("password", this.password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", this.token);
            jsonObject.put("password", this.password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
