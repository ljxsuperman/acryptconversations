package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by liujianxu on 10/29/15.
 */
public class CommonRespBean implements Serializable, JsonFormat {

    protected Integer errcode = 0;

    protected String errmsg = "";

    public CommonRespBean(){

    }

    public CommonRespBean(Integer errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public CommonRespBean(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.errcode = Integer.valueOf(jsonObject.getString("errcode"));
            this.errmsg = jsonObject.getString("errmsg");
        } catch (JSONException e) {
            e.printStackTrace();
            this.errcode = 0;
            this.errmsg = "";
        }
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.errcode);
            jsonObject.put("errmsg", this.errmsg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.errcode);
            jsonObject.put("errmsg", this.errmsg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
