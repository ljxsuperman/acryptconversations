package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by liujianxu on 10/29/15.
 */
public class LoginAccountRespBean extends CommonRespBean {

    private CommonRespBean commonRespBean = null;

    private String loginToken = "";

    private UserInfo userinfo = null;

    public LoginAccountRespBean(){

    }

    public LoginAccountRespBean(CommonRespBean commonRespBean, String loginToken, UserInfo userinfo) {
        this.commonRespBean = commonRespBean;
        this.loginToken = loginToken;
        this.userinfo = userinfo;
    }

    public LoginAccountRespBean(Integer errcode, String errmsg, CommonRespBean commonRespBean, String loginToken, UserInfo userinfo) {
        super(errcode, errmsg);
        this.commonRespBean = commonRespBean;
        this.loginToken = loginToken;
        this.userinfo = userinfo;
    }

    public LoginAccountRespBean(String jsonString, CommonRespBean commonRespBean, String loginToken, UserInfo userinfo) {
        super(jsonString);
        this.commonRespBean = commonRespBean;
        this.loginToken = loginToken;
        this.userinfo = userinfo;
    }

    public LoginAccountRespBean(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.commonRespBean = new CommonRespBean(jsonString);
            if(jsonObject.has("logintoken")) {
                this.loginToken = jsonObject.getString("logintoken");
            }
            if(jsonObject.has("userinfo")) {
                this.userinfo = new UserInfo(jsonObject.getJSONObject("userinfo").toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
            this.commonRespBean = null;
            this.loginToken = "";
            this.userinfo = null;
        }
    }

    public CommonRespBean getCommonRespBean() {
        return commonRespBean;
    }

    public void setCommonRespBean(CommonRespBean commonRespBean) {
        this.commonRespBean = commonRespBean;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public UserInfo getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(UserInfo userinfo) {
        this.userinfo = userinfo;
    }

    @Override
    public Integer getErrcode() {
        return this.commonRespBean.getErrcode();
    }

    @Override
    public void setErrcode(Integer errcode) {
        this.commonRespBean.setErrcode(errcode);
    }

    @Override
    public String getErrmsg() {
        return this.commonRespBean.getErrmsg();
    }

    @Override
    public void setErrmsg(String errmsg) {
        this.commonRespBean.setErrmsg(errmsg);
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.commonRespBean.getErrcode());
            jsonObject.put("errmsg", this.commonRespBean.getErrmsg());
            jsonObject.put("logintoken", this.loginToken);
            jsonObject.put("userinfo", this.userinfo.toJsonObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.commonRespBean.getErrcode());
            jsonObject.put("errmsg", this.commonRespBean.getErrmsg());
            jsonObject.put("logintoken", this.loginToken);
            jsonObject.put("userinfo", this.userinfo.toJsonObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
