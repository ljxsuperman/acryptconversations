package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by liujianxu on 11/2/15.
 */
public class ResetPasswordVcodeSendRespBean extends CommonRespBean {

    private CommonRespBean commonRespBean;

    /*这是申请短信验证码的临时token,不是loginToken*/
    private String token = "";

    public ResetPasswordVcodeSendRespBean(){

    }

    public ResetPasswordVcodeSendRespBean(CommonRespBean commonRespBean, String token) {
        this.commonRespBean = commonRespBean;
        this.token = token;
    }

    public ResetPasswordVcodeSendRespBean(Integer errcode, String errmsg, CommonRespBean commonRespBean, String token) {
        super(errcode, errmsg);
        this.commonRespBean = commonRespBean;
        this.token = token;
    }

    public ResetPasswordVcodeSendRespBean(String jsonString, CommonRespBean commonRespBean, String token) {
        super(jsonString);
        this.commonRespBean = commonRespBean;
        this.token = token;
    }

    public ResetPasswordVcodeSendRespBean(String jsonString){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.commonRespBean = new CommonRespBean(jsonString);
            this.token = jsonObject.getString("token");
        } catch (JSONException e) {
            e.printStackTrace();
            this.commonRespBean = null;
            this.token = "";
        }
    }

    public CommonRespBean getCommonRespBean() {
        return commonRespBean;
    }

    public void setCommonRespBean(CommonRespBean commonRespBean) {
        this.commonRespBean = commonRespBean;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String getErrmsg() {
        return this.commonRespBean.getErrmsg();
    }

    @Override
    public void setErrmsg(String errmsg) {
        this.commonRespBean.setErrmsg(errmsg);
    }

    @Override
    public Integer getErrcode() {
        return this.commonRespBean.getErrcode();
    }

    @Override
    public void setErrcode(Integer errcode) {
        this.commonRespBean.setErrcode(errcode);
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.commonRespBean.getErrcode());
            jsonObject.put("errmsg", this.commonRespBean.getErrmsg());
            jsonObject.put("token", this.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.commonRespBean.getErrcode());
            jsonObject.put("errmsg", this.commonRespBean.getErrmsg());
            jsonObject.put("token", this.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
