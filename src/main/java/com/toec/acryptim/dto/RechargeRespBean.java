package com.toec.acryptim.dto;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by liujianxu on 11/2/15.
 */
public class RechargeRespBean extends CommonRespBean {

    private CommonRespBean commonRespBean = null;

    private UserInfo userinfo = null;

    public RechargeRespBean(){

    }

    public RechargeRespBean(CommonRespBean commonRespBean, UserInfo userinfo) {
        this.commonRespBean = commonRespBean;
        this.userinfo = userinfo;
    }

    public RechargeRespBean(Integer errcode, String errmsg, CommonRespBean commonRespBean, UserInfo userinfo) {
        super(errcode, errmsg);
        this.commonRespBean = commonRespBean;
        this.userinfo = userinfo;
    }

    public RechargeRespBean(String jsonString, CommonRespBean commonRespBean, UserInfo userinfo) {
        super(jsonString);
        this.commonRespBean = commonRespBean;
        this.userinfo = userinfo;
    }

    public RechargeRespBean(String jsonString){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            this.commonRespBean = new CommonRespBean(jsonString);
            this.userinfo = new UserInfo(jsonObject.getJSONObject("userinfo").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public CommonRespBean getCommonRespBean() {
        return commonRespBean;
    }

    public void setCommonRespBean(CommonRespBean commonRespBean) {
        this.commonRespBean = commonRespBean;
    }

    public UserInfo getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(UserInfo userinfo) {
        this.userinfo = userinfo;
    }

    @Override
    public Integer getErrcode() {
        return super.getErrcode();
    }

    @Override
    public void setErrcode(Integer errcode) {
        super.setErrcode(errcode);
    }

    @Override
    public String getErrmsg() {
        return super.getErrmsg();
    }

    @Override
    public void setErrmsg(String errmsg) {
        super.setErrmsg(errmsg);
    }

    @Override
    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.commonRespBean.getErrcode());
            jsonObject.put("errmsg", this.commonRespBean.getErrmsg());
            jsonObject.put("userinfo", this.userinfo.toJsonObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errcode", this.commonRespBean.getErrcode());
            jsonObject.put("errmsg", this.commonRespBean.getErrmsg());
            jsonObject.put("userinfo", this.userinfo.toJsonObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
