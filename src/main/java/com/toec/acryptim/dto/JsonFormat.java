package com.toec.acryptim.dto;

import org.json.JSONObject;

/**
 * Created by liujianxu on 10/29/15.
 */
public interface JsonFormat {

    public String toJsonString();

    public JSONObject toJsonObject();

}
