package com.toec.acryptim.biz.impl;

import com.toec.acryptim.biz.IAccountBiz;
import com.toec.acryptim.dao.IAccountDao;
import com.toec.acryptim.dao.IUserPropertyDao;
import com.toec.acryptim.dao.impl.AccountDaoImpl;
import com.toec.acryptim.dao.impl.UserPropertyDaoImpl;
import com.toec.acryptim.entity.Account;
import com.toec.acryptim.entity.UserProperty;
import com.toec.acryptim.environment.ACryptIMApplication;
import com.toec.acryptim.environment.CommonSettings;
import com.toec.acryptim.utils.FileUtils;
import com.toec.acryptim.utils.SharedPrefsUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by liujianxu on 11/6/15.
 */
public class AccountBizImpl implements IAccountBiz {

    private IAccountDao accountDao = null;

    private IUserPropertyDao userPropertyDao = null;

    public AccountBizImpl(){
        this.accountDao = AccountDaoImpl.getInstance();
        this.userPropertyDao = UserPropertyDaoImpl.getInstance();
    }

    @Override
    public void applyAccountInfo(String number, String passwordInSHA1, String nickname, byte[] avator) {
        SharedPrefsUtil.saveAccountNumber(ACryptIMApplication.getInstance(), number);
        SharedPrefsUtil.saveAccountPasswordInSHA1(ACryptIMApplication.getInstance(), passwordInSHA1);
        SharedPrefsUtil.saveAccountNickname(ACryptIMApplication.getInstance(), passwordInSHA1);
        if(avator != null && avator.length != 0) {
            String avatorPath = String.format(CommonSettings.USER_AVATOR_UNFORMATTED_PATH, number);
            SharedPrefsUtil.saveAccountAvatorPath(ACryptIMApplication.getInstance(), avatorPath);
            try {
                FileUtils.writeToFile(avator, avatorPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateLoginInfo(String number, long expireDate, int validdays, String loginToken, boolean isLogin) {
        accountDao.prepareAccountData(number);
        Account account = accountDao.findByNumber(number);
        if(account == null){
            account = new Account();
            account.setNumber(number);
        }
        account.setExpireTimestamp(expireDate);
        account.setLeftTimeInMiSec(validdays * 86400 * 1000L);
        account = accountDao.save(account);
        ACryptIMApplication.getInstance().setCurrentAccount(account);
        if(isLogin){
            account.setLoginCount(account.getLoginCount() + 1);
            account.setLastLoginTimestamp(System.currentTimeMillis());
        }

        List<UserProperty> userProperties = (List<UserProperty>)userPropertyDao.getAllPropertyByAccountNumber(number);
        if(userProperties == null || userProperties.isEmpty()) {
            userProperties = new ArrayList<>();
            UserProperty avatorPathProperty = new UserProperty(
                    UUID.randomUUID().toString(),
                    Account.ACCOUNT_PREF_KEY_AVATOR_PATH,
                    UserProperty.Type.TYPE_CHAR_SEQUENCE,
                    SharedPrefsUtil.getAccountAvatorPath(ACryptIMApplication.getInstance()),
                    number
            );
            userPropertyDao.save(avatorPathProperty);
            userProperties.add(avatorPathProperty);
            UserProperty nicknameProperty = new UserProperty(
                    UUID.randomUUID().toString(),
                    Account.ACCOUNT_PREF_KEY_AVATOR_PATH,
                    UserProperty.Type.TYPE_CHAR_SEQUENCE,
                    SharedPrefsUtil.getAccountNickname(ACryptIMApplication.getInstance()),
                    number
            );
            userPropertyDao.save(nicknameProperty);
            userProperties.add(nicknameProperty);
        }
        for(UserProperty up : userProperties) {
            account.addProperty(up);
        }
        accountDao.update(account);

        SharedPrefsUtil.saveLoginToken(loginToken, ACryptIMApplication.getInstance());
        ACryptIMApplication.getInstance().setCurrentAccount(account);
    }

    private static class SingletonHolder{
        private final static AccountBizImpl instance = new AccountBizImpl();
    }

    public static AccountBizImpl getInstance() {
        return SingletonHolder.instance;
    }

}
