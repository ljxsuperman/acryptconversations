package com.toec.acryptim.biz.impl;

import android.text.TextUtils;

import com.toec.acryptim.biz.IPingyingBiz;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;

/**
 * Created by liujianxu on 11/9/15.
 */
public class PingYingBizImpl implements IPingyingBiz {

    public static HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();

    @Override
    public String convertStringToPinyin(String originalStr, boolean isFirstLetterUpcase) {
        StringBuilder pinyinString = new StringBuilder("");
        char[] charArray = originalStr.toCharArray();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        defaultFormat.setVCharType(HanyuPinyinVCharType.WITH_V);

        try {
            for (int i = 0; i < charArray.length; i++) {
                if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    String[] temp = null;
                    if(i == 0){
                        temp[0] = changeStrPinYin(charArray[0], false, isFirstLetterUpcase);
                    } else {
                        temp = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                    }
                    if (temp == null || TextUtils.isEmpty(temp[0])) {
                        continue;
                    }
                    if(isFirstLetterUpcase) {
                        pinyinString.append(temp[0].replaceFirst(temp[0].substring(0, 1), temp[0].substring(0, 1).toUpperCase()));
                    }else{
                        pinyinString.append(temp[0]);
                    }
                } else {
                    pinyinString.append(Character.toString(charArray[i]));
                }
            }
        }catch (Exception e){

        }

        return pinyinString.toString();
    }

    @Override
    public String convertStringToPinyinWithTone(String originalStr, boolean isFirstLetterUpcase) {
        StringBuilder pinyinString = new StringBuilder("");
        char[] charArray = originalStr.toCharArray();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
        defaultFormat.setVCharType(HanyuPinyinVCharType.WITH_V);

        try {
            for (int i = 0; i < charArray.length; i++) {
                if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    String[] temp = null;
                    if(i == 0){
                        temp[0] = changeStrPinYin(charArray[0], true, isFirstLetterUpcase);
                    } else {
                        temp = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                    }
                    if (temp == null || TextUtils.isEmpty(temp[0])) {
                        continue;
                    }
                    if(isFirstLetterUpcase) {
                        pinyinString.append(temp[0].replaceFirst(temp[0].substring(0, 1), temp[0].substring(0, 1).toUpperCase()));
                    }else{
                        pinyinString.append(temp[0]);
                    }
                } else {
                    pinyinString.append(Character.toString(charArray[i]));
                }
            }
        }catch (Exception e){

        }

        return pinyinString.toString();
    }

    // 多音字处理
    private String changeStrPinYin(char ch, boolean withTone, boolean isFirstLetterUpcase) {
        StringBuilder pinyin = new StringBuilder();

        if (ch == '重') {
            pinyin.append("chong");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '区') {
            pinyin.append("ou");
            if(withTone){
                pinyin.append("1");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '仇') {
            pinyin.append("qiu");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '秘') {
            pinyin.append("bi");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '冼') {
            pinyin.append("xian");
            if(withTone){
                pinyin.append("3");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '解') {
            pinyin.append("xie");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '折') {
            pinyin.append("she");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '单') {
            pinyin.append("shan");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '朴') {
            pinyin.append("piao");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '翟') {
            pinyin.append("zhai");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '查') {
            pinyin.append("zha");
            if(withTone){
                pinyin.append("1");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '尉') {
            pinyin.append("yu");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '乐') {
            pinyin.append("yue");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '覃') {
            pinyin.append("tan");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '瞿') {
            pinyin.append("qu");
            if(withTone){
                pinyin.append("2");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '藏') {
            pinyin.append("zang");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '都') {
            pinyin.append("du");
            if(withTone){
                pinyin.append("1");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '贲') {
            pinyin.append("ben");
            if(withTone){
                pinyin.append("1");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '阚') {
            pinyin.append("kan");
            if(withTone){
                pinyin.append("4");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }
        if (ch == '曾') {
            pinyin.append("zeng");
            if(withTone){
                pinyin.append("1");
            }
            if(isFirstLetterUpcase) {
                pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase());
            }
            return pinyin.toString();
        }

        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
        if(withTone) {
            defaultFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
        }else{
            defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        }
        try {
            pinyin.append(PinyinHelper.toHanyuPinyinStringArray(ch, defaultFormat)[0]);
            if (isFirstLetterUpcase) {
                return pinyin.replace(0, 1, pinyin.substring(0, 1).toUpperCase()).toString();
            } else {
                return pinyin.toString();
            }
        }catch (Exception e){
            return "";
        }
    }

    @Override
    public String convertStringToPinyinIndex(String originalStr, boolean isAllUpcase) {
        StringBuilder pinyinString = new StringBuilder("");
        char[] charArray = originalStr.toCharArray();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
        defaultFormat.setVCharType(HanyuPinyinVCharType.WITH_V);

        try {
            for (int i = 0; i < charArray.length; i++) {
                if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    String[] temp = null;
                    if(i == 0){
                        temp[0] = changeStrPinYin(charArray[0], true, true);
                    } else {
                        temp = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                    }
                    if (temp == null || TextUtils.isEmpty(temp[0])) {
                        continue;
                    }
                    if(isAllUpcase) {
                        pinyinString.append(temp[0].replaceFirst(temp[0].substring(0, 1), temp[0].substring(0, 1).toUpperCase()).charAt(0));
                    }else{
                        pinyinString.append(temp[0].charAt(0));
                    }
                } else {
                    pinyinString.append(Character.toString(charArray[i]));
                }
            }
        }catch (Exception e){

        }

        return pinyinString.toString();
    }

    @Override
    public char getPinyinStringFirstLetter(String originalStr, boolean isUpcase) {
        if(originalStr == null || originalStr.isEmpty()){
            return 0;
        }
        char firstChar = originalStr.charAt(0);
        char result = changeStrPinYin(firstChar, false, isUpcase).charAt(0);
        if(result >= '0' && result <= '9'){
            return '#';
        }else{
            return result;
        }
    }

    private static class SingletonHolder{
        private static final PingYingBizImpl instance = new PingYingBizImpl();
    }

    public static PingYingBizImpl getInstance(){
        return SingletonHolder.instance;
    }
}
