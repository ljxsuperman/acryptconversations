package com.toec.acryptim.biz.impl;

import android.os.Bundle;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.toec.acryptim.biz.IAccountNetBiz;
import com.toec.acryptim.dto.CommonRespBean;
import com.toec.acryptim.dto.LoginAccountReqBean;
import com.toec.acryptim.dto.LoginAccountRespBean;
import com.toec.acryptim.dto.RechargeRespBean;
import com.toec.acryptim.dto.RegUserRegistReqBean;
import com.toec.acryptim.dto.RegUserRegistRespBean;
import com.toec.acryptim.dto.RegUserVcodeCheckReqBean;
import com.toec.acryptim.dto.RegUserVcodeSendReqBean;
import com.toec.acryptim.dto.RegUserVcodeSendRespBean;
import com.toec.acryptim.dto.ResetPasswordReqBean;
import com.toec.acryptim.dto.ResetPasswordVcodeCheckReqBean;
import com.toec.acryptim.dto.ResetPasswordVcodeSendReqBean;
import com.toec.acryptim.dto.ResetPasswordVcodeSendRespBean;
import com.toec.acryptim.net.AsyncHttpClientCreator;
import com.toec.acryptim.net.HttpConnetorCallback;
import com.toec.acryptim.net.Network;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by liujianxu on 10/29/15.
 */
public class AccountNetBizImpl implements IAccountNetBiz {

    private AccountNetBizImpl(){

    }

    @Override
    public void loginAccount(String username, String passwordInSha1, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        LoginAccountReqBean loginAccountReqBean = new LoginAccountReqBean(username, passwordInSha1);
        HttpEntity httpEntity = new ByteArrayEntity(loginAccountReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_LOGIN_URL, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                callback.onStart();
            }

            @Override
            public void onCancel() {
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Bundle data = new Bundle();
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                LoginAccountRespBean loginAccountRespBean = new LoginAccountRespBean(jsonObject.toString());
                if(loginAccountRespBean.getCommonRespBean() == null){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    data.putSerializable("userinfo", loginAccountRespBean.getUserinfo());
                    data.putString("logintoken", loginAccountRespBean.getLoginToken());
                    callback.onSuccess(statusCode, loginAccountRespBean.getErrcode(), loginAccountRespBean.getErrmsg(), data);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void logoutAccount(String loginToken, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        asyncHttpClient.get(Network.KMC_LOGOUT_URL + "?logintoken=" + loginToken, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                CommonRespBean commonRespBean = new CommonRespBean(jsonObject.toString());
                if(commonRespBean.getErrmsg().equals("")){
                    isRespValid = false;
                }
                if(!isRespValid){
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    callback.onSuccess(statusCode, commonRespBean.getErrcode(), commonRespBean.getErrmsg(), null);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void regUserVcodeSend(String username, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        RegUserVcodeSendReqBean regUserVcodeSendReqBean = new RegUserVcodeSendReqBean(username, false);
        HttpEntity httpEntity = new ByteArrayEntity(regUserVcodeSendReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_REG_USER_VCODE_SEND, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler(){

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Bundle data = new Bundle();
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                RegUserVcodeSendRespBean regUserVcodeSendRespBean = new RegUserVcodeSendRespBean(jsonObject.toString());
                if(regUserVcodeSendRespBean.getCommonRespBean() == null){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    data.putString("token", regUserVcodeSendRespBean.getToken());
                    callback.onSuccess(statusCode, regUserVcodeSendRespBean.getErrcode(), regUserVcodeSendRespBean.getErrmsg(), data);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void regUserVcodeCheck(String token, String vcode, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        RegUserVcodeCheckReqBean regUserVcodeCheckReqBean = new RegUserVcodeCheckReqBean(token, vcode);
        HttpEntity httpEntity = new ByteArrayEntity(regUserVcodeCheckReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_REG_USER_VCODE_CHECK, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler(){

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                CommonRespBean commonRespBean = new CommonRespBean(new String(responseBody));
                if(commonRespBean.getErrmsg().equals("")){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    callback.onSuccess(statusCode, commonRespBean.getErrcode(), commonRespBean.getErrmsg(), null);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void regUserRegist(String token, String passwordInSha1, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        RegUserRegistReqBean regUserRegistReqBean = new RegUserRegistReqBean(token, passwordInSha1);
        HttpEntity httpEntity = new ByteArrayEntity(regUserRegistReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_REG_USER_REGIST, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler(){

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Bundle data = new Bundle();
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                RegUserRegistRespBean regUserRegistRespBean = new RegUserRegistRespBean(jsonObject.toString());
                if(regUserRegistRespBean.getCommonRespBean() == null){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    data.putSerializable("userinfo", regUserRegistRespBean.getUserinfo());
                    callback.onSuccess(statusCode, regUserRegistRespBean.getErrcode(), regUserRegistRespBean.getErrmsg(), data);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void resetPasswrodVcodeSend(String username, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        ResetPasswordVcodeSendReqBean resetPasswordVcodeSendReqBean = new ResetPasswordVcodeSendReqBean(username, false);
        HttpEntity httpEntity = new ByteArrayEntity(resetPasswordVcodeSendReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_RESET_PASSWORD_VCODE_SEND, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler(){

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Bundle data = new Bundle();
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                ResetPasswordVcodeSendRespBean resetPasswordVcodeSendRespBean = new ResetPasswordVcodeSendRespBean(new String(responseBody));
                if(resetPasswordVcodeSendRespBean.getCommonRespBean() == null){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    data.putString("token", resetPasswordVcodeSendRespBean.getToken());
                    callback.onSuccess(statusCode, resetPasswordVcodeSendRespBean.getErrcode(), resetPasswordVcodeSendRespBean.getErrmsg(), data);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void resetPasswordVcodeCheck(String token, String vcode, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        ResetPasswordVcodeCheckReqBean resetPasswordVcodeCheckReqBean = new ResetPasswordVcodeCheckReqBean(token, vcode);
        HttpEntity httpEntity = new ByteArrayEntity(resetPasswordVcodeCheckReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_RESET_PASSWORD_VCODE_CHECK, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler(){

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                CommonRespBean commonRespBean = new CommonRespBean(new String(responseBody));
                if(commonRespBean.getErrmsg().equals("")){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    callback.onSuccess(statusCode, commonRespBean.getErrcode(), commonRespBean.getErrmsg(), null);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void resetPassword(String token, String passwordInSha1, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        ResetPasswordReqBean resetPasswordReqBean = new ResetPasswordReqBean(token, passwordInSha1);
        HttpEntity httpEntity = new ByteArrayEntity(resetPasswordReqBean.toJsonString().getBytes());
        asyncHttpClient.post(null, Network.KMC_RESET_PASSWORD, httpEntity, Network.HEAD_CONTENT_TYPE_JSON, new AsyncHttpResponseHandler(){

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                CommonRespBean commonRespBean = new CommonRespBean(new String(responseBody));
                if(commonRespBean.getErrmsg().equals("")){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    callback.onSuccess(statusCode, commonRespBean.getErrcode(), commonRespBean.getErrmsg(), null);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void recharge(String username, String rcode, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        asyncHttpClient.post(Network.KMC_RECHARGE + "?username=" + username + "&rcode=" + rcode, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Bundle data = new Bundle();
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                RechargeRespBean rechargeRespBean = new RechargeRespBean(jsonObject.toString());
                if(rechargeRespBean.getCommonRespBean() == null){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    data.putSerializable("userinfo", rechargeRespBean.getUserinfo());
                    callback.onSuccess(statusCode, rechargeRespBean.getErrcode(), rechargeRespBean.getErrmsg(), data);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    @Override
    public void changePassword(String loginToken, String oldPasswordInSha1, String passwordInSha1, final HttpConnetorCallback callback) {
        AsyncHttpClient asyncHttpClient = AsyncHttpClientCreator.create();
        asyncHttpClient.post(Network.KMC_CHANGE_PASSWORD + "?logintoken=" + loginToken, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                callback.onStart();
            }

            @Override
            public void onCancel() {
                super.onCancel();
                callback.onCancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JSONObject jsonObject = null;
                boolean isRespValid = true;
                try {
                    jsonObject = new JSONObject(new String(responseBody));
                } catch (JSONException e) {
                    e.printStackTrace();
                    isRespValid = false;
                }
                CommonRespBean commonRespBean = new CommonRespBean(jsonObject.toString());
                if(commonRespBean.getErrmsg().equals("")){
                    isRespValid = false;
                    callback.onRespInvalid(statusCode, new String(responseBody));
                } else {
                    callback.onSuccess(statusCode, commonRespBean.getErrcode(), commonRespBean.getErrmsg(), null);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody == null){
                    responseBody = new byte[]{};
                }
                callback.onFailure(statusCode, new String(responseBody));
            }
        });
    }

    private static class SingletonHolder{
        private final static AccountNetBizImpl instance = new AccountNetBizImpl();
    }

    public static AccountNetBizImpl getInstance() {
        return SingletonHolder.instance;
    }
}
