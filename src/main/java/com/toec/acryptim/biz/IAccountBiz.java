package com.toec.acryptim.biz;

/**
 * Created by liujianxu on 11/3/15.
 */
public interface IAccountBiz {

    void applyAccountInfo(String username, String passwordInSHA1, String nickname, byte[] avator);

    void updateLoginInfo(String username, long expireDate, int validdays, String loginToken, boolean isLogin);

}
