package com.toec.acryptim.biz;

import android.provider.ContactsContract;

/**
 * Created by liujianxu on 11/9/15.
 */
public interface IPingyingBiz {

    String[] PHONES_PROJECTION = new String[] { ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER };

    String STRS[] = { "0", "1", "[2abc]", "[3def]", "[4ghi]", "[5jkl]", "[6mno]", "[7pqrs]", "[8tuv]", "[9wxyz]" };

    String convertStringToPinyin(String originalStr, boolean isFirstLetterUpcase);

    String convertStringToPinyinWithTone(String originalStr, boolean isFirstLetterUpcase);

    /**
     * 将拼音转化为拼音首字母缩写,如果是汉字和字母数字混排,则保持数字或字母不变
     */
    String convertStringToPinyinIndex(String originalStr, boolean isAllUpcase);

    /**
     * 将原文转换为拼音后取首字母作为排序依据索引
     * @param originalStr 原文,可以是汉字\数字\字母的混合
     * @param isUpcase 结果是大小写的开关
     * @return 首字母 char类型
     */
    char getPinyinStringFirstLetter(String originalStr, boolean isUpcase);

}
