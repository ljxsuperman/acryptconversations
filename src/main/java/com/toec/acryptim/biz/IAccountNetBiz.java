package com.toec.acryptim.biz;

import com.toec.acryptim.net.HttpConnetorCallback;

/**
 * Created by liujianxu on 10/29/15.
 */
public interface IAccountNetBiz {

    void loginAccount(String username, String passwordInSha1, HttpConnetorCallback callback);

    void logoutAccount(String loginToken, HttpConnetorCallback callback);

    void regUserVcodeSend(String username, HttpConnetorCallback callback);

    void regUserVcodeCheck(String token, String vcode, HttpConnetorCallback callback);

    void regUserRegist(String token, String passwordInSha1, HttpConnetorCallback callback);

    void resetPasswrodVcodeSend(String username, HttpConnetorCallback callback);

    void resetPasswordVcodeCheck(String token, String vcode, HttpConnetorCallback callback);

    void resetPassword(String token, String passwordInSha1, HttpConnetorCallback callback);

    void recharge(String username, String rcode, HttpConnetorCallback callback);

    void changePassword(String loginToken, String oldPasswordInSha1, String passwordInSha1, HttpConnetorCallback callback);

}
