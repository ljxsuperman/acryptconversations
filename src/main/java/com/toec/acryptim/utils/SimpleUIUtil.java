package com.toec.acryptim.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.toec.acryptim.R;

/**
 * Created by root on 11/16/15.
 */
public class SimpleUIUtil {

    private static Toast toast;

    private static ProgressDialog progressDialog;

    public synchronized static void showProgressDialog(Context context, Integer theme, String title, String message, boolean cancelalbe){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.cancel();
            }
            progressDialog = null;
        }
        if(progressDialog == null){
            if(theme == null){
                progressDialog = new ProgressDialog(context);
            }else {
                progressDialog = new ProgressDialog(context, theme);
            }
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(cancelalbe);
            progressDialog.show();
        }
    }

    public static void dismissProgressDialog(){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.cancel();
            }
            progressDialog = null;
        }
    }

    public synchronized static void showProgressDialog(Context context, Integer theme, int titleResID, int messageResID, boolean cancelalbe){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.cancel();
            }
            progressDialog = null;
        }
        if(progressDialog == null){
            if(theme == null){
                progressDialog = new ProgressDialog(context);
            }else {
                progressDialog = new ProgressDialog(context, theme);
            }
            progressDialog.setTitle(context.getResources().getString(titleResID));
            progressDialog.setMessage(context.getResources().getString(messageResID));
            progressDialog.setCancelable(cancelalbe);
            progressDialog.show();
        }
    }

    public synchronized static void showCommonProgressDialog(Context context){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.cancel();
            }
            progressDialog = null;
        }
        if(progressDialog == null){
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(context.getResources().getString(R.string.common_progress_dialog_title));
            progressDialog.setMessage(context.getResources().getString(R.string.common_progress_dialog_content));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public synchronized static void showLongToast(Context context, String msg){
        if(toast != null){
            toast.cancel();
            toast = null;
        }
        if(toast == null){
            toast = new Toast(context);
        }
        toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public synchronized static void showLongToast(Context context, int msgResID){
        if(toast != null){
            toast.cancel();
            toast = null;
        }
        if(toast == null){
            toast = new Toast(context);
        }
        toast.makeText(context, msgResID, Toast.LENGTH_LONG).show();
    }

    public synchronized static void showShortToast(Context context, String msg){
        if(toast != null){
            toast.cancel();
            toast = null;
        }
        if(toast == null){
            toast = new Toast(context);
        }
        toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public synchronized static void showShortToast(Context context, int msgResID){
        if(toast != null){
            toast.cancel();
            toast = null;
        }
        if(toast == null){
            toast = new Toast(context);
        }
        toast.makeText(context, msgResID, Toast.LENGTH_SHORT).show();
    }

}
