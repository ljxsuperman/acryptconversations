package com.toec.acryptim.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.toec.acryptim.environment.ACryptIMApplication;
import com.toec.acryptim.environment.CommonSettings;

/**
 * 作者：萧文翰
 * 邮箱：wh1990xiao2005@hotmail.com
 * 移动电话：+86 15822932537
 * 微博：http://weibo.com/xwhnew
 * 个人网站：http://www.xwhcoder.com
 */
public class SharedPrefsUtil {

    private static SharedPreferences sharedPreferences;

    /**
     * 设置是否跳过登录提示
     *
     * @param isSkip  需要显示则为false；反之为true
     * @param context context
     */
    public static void setLoginTipSkipNextTime(Context context, boolean isSkip) {
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        sharedPreferences.edit().putBoolean(CommonSettings.PREF_LOGIN_TIP_SKIP_NEXT_TIME, isSkip).commit();
    }

    /**
     * 获取是否跳过登录提示
     *
     * @param context context
     * @return 需要提示则为false；反之为true。默认为false。
     */
    public static boolean getLoginTipSkipNextTime(Context context) {
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getBoolean(CommonSettings.PREF_LOGIN_TIP_SKIP_NEXT_TIME, CommonSettings.DEFAULT_LOGIN_TIP_SKIP_NEXT_TIME);
    }

    public static void saveAccountNumber(Context context, String number){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        if(number == null){
            number = "";
        }
        sharedPreferences.edit().putString(CommonSettings.PREF_ACCOUNT_NUMBER, number).commit();
    }

    public static String getAccountNumber(Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString(CommonSettings.PREF_ACCOUNT_NUMBER, CommonSettings.DEFAULT_ACCOUNT_NUMBER);
    }

    public static void saveAccountNickname(Context context, String nickname){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        if(nickname == null){
            nickname = "";
        }
        sharedPreferences.edit().putString(CommonSettings.PREF_ACCOUNT_NICKNAME, nickname).commit();
    }

    public static String getAccountNickname(Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString(CommonSettings.PREF_ACCOUNT_NICKNAME, CommonSettings.DEFALUT_ACCOUNT_NICKNAME);
    }

    public static void saveAccountPasswordInSHA1(Context context, String passwordInSHA1){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        if(passwordInSHA1 == null){
            passwordInSHA1 = "";
        }
        sharedPreferences.edit().putString(CommonSettings.PREF_ACCOUNT_PASSWORD, passwordInSHA1).commit();
    }

    public static String getAccountPasswordInSHA1(Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString(CommonSettings.PREF_ACCOUNT_PASSWORD, CommonSettings.DEFAULT_ACCOUNT_PASSWORD);
    }

    public static void saveAccountAvatorPath(Context context, String avatorPath){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        if(avatorPath == null){
            avatorPath = "";
        }
        sharedPreferences.edit().putString(CommonSettings.PREF_ACCOUNT_AVATOR_PATH, avatorPath).commit();
    }

    public static String getAccountAvatorPath(Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString(CommonSettings.PREF_ACCOUNT_AVATOR_PATH, CommonSettings.DEFAULT_ACCOUNT_AVATOR_PATH);
    }

    public static void saveLoginToken(String loginToken, Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        sharedPreferences.edit().putString(CommonSettings.PREF_LOGIN_TOKEN, loginToken);
    }

    public static String getLoginToken(Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString(CommonSettings.PREF_LOGIN_TOKEN, CommonSettings.DEFAULT_LOGIN_TOKEN);
    }

    public static void saveTmpToken(String tmpToken, Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        sharedPreferences.edit().putString(CommonSettings.PREF_TMP_TOKEN, tmpToken);
    }

    public static String getTmpToken(Context context){
        if(context == null){
            context = ACryptIMApplication.getInstance();
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString(CommonSettings.PREF_TMP_TOKEN, CommonSettings.DEFAULT_TMP_TOKEN);
    }

}
