package com.toec.acryptim.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.io.Serializable;

/**
 * Created by liujianxu on 10/29/15.
 */
public class CommonUtil {

    public static String getPackageName(Context context){
        return context.getPackageName();
    }

    public static int getVersionCode(Context context){
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getVersionName(Context context){
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final int getDatabaseVersion(Context context){
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return Integer.valueOf(packageInfo.versionName.replace(".", ""));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String listArray(Class<? extends Serializable> clazz, Object[] array){
        if(array == null || array.length == 0){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < array.length; ++i){
            sb.append("'" + clazz.cast(array[i]).toString() + "'");
            if(i != array.length - 1){
                sb.append(", ");
            }
        }
        return sb.toString();
    }

}
