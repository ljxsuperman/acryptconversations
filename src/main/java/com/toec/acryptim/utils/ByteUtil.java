package com.toec.acryptim.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ByteUtil {

	public static final String HEX_STRING = "0123456789ABCDEF";

	public static String byteToHex(byte b) {
		return ("0x" + HEX_STRING.charAt(0xf & b >> 4) + HEX_STRING.charAt(b & 0xf));
	}

	public static byte hexToByte(String s){
		if(s == null || s.length() <= 1){
			return 0x00;
		}
		if(s.length() > 2) {
			s = s.substring(0, 2);
		}
		return (byte) Integer.parseInt(s, 16);
	}

	public static String byteArrayToHexString(byte[] b){
		if(b.length == 0) return "";
		StringBuilder sb = new StringBuilder("");
		for(int i = 0; i < b.length; ++i){
			sb = sb.append(HEX_STRING.charAt(0xf & b[i] >> 4) + "" + HEX_STRING.charAt(b[i] & 0xf));
		}
		return sb.toString();
	}
	
	public static byte[] hexStringToByteArray(String s){
		if(s.isEmpty()) return new byte[]{};
		StringBuilder sb = new StringBuilder(s);
		byte[] result = new byte[s.length() / 2];
		for(int i = 0; i < result.length; ++i){
			result[i] = (byte) Integer.parseInt(sb.substring(i*2, i*2+2), 16);
		}
		return result;
	}

	public static String[] byteArrayToHexArray(Byte[] b) {
		String[] stringArr = new String[b.length];
		for (int i = 0; i < b.length; i++) {
			stringArr[i] = byteToHex(b[i]);
		}
		return stringArr;
	}

	public static byte[] sha1(byte[] b) {
		MessageDigest sha1 = null;
		try {
			sha1 = MessageDigest.getInstance("sha1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		sha1.update(b);
		byte[] hash = sha1.digest();
		return hash;
	}
	
	public static String sha1(String s){
		byte[] orgBytes = s.getBytes();
		byte[] sha1Bytes = sha1(orgBytes);
		String result = byteArrayToHexString(sha1Bytes);
		return result;
	}
	
	public static void showByteArray(byte[] b){
		StringBuilder sb = new StringBuilder("");
		sb.append("[");
		for(int i = 0; i < b.length; ++i) {
			if(i != b.length - 1){
				sb.append(b[i] + ", ");
			} else {
				sb.append(b[i]);
			}
		}
		sb.append("]");
	}
	
	public static String showByteArrayByHex(byte[] b, int num){
		if(num < 0)
			num = b.length;
		if(b == null)
			return "null";
		if(num > b.length)
			num = b.length;
		StringBuilder sb = new StringBuilder("");
		sb.append("[");
		for(int i = 0; i < num; ++i) {
			if(i != num - 1){
				sb.append(byteToHex(b[i]) + ", ");
			} else {
				sb.append(byteToHex(b[i]));
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	public static String showByteArrayByHex(List<Byte> b, int num){
		byte[] bArray = new byte[b.size()];
		for(int i = 0; i < b.size(); ++i){
			bArray[i] = b.get(i);
		}
		return showByteArrayByHex(bArray, num);
	}
	
	public static byte[] getRsp(byte[] bOutData, int retLength){
		byte[] rsp = new byte[retLength];
		System.arraycopy(bOutData, 0, rsp, 0, retLength);
//		for(int i = 0; i < retLength; ++i){
//			rsp[i] = bOutData[i];
//		}
		return rsp;
	}
	
	public static boolean byteArrayComp(byte[] rsp, byte[] org){
		if(rsp.length != org.length){
			return false;
		}
		int length = rsp.length;
		for(int i = 0; i < length; ++i){
			if(rsp[i] != org[i]){
				return false;
			}
		}
		return true;
	}
	
	public static boolean byteArrayStartWith(byte[] byteArray, byte[] startBytes){
		boolean result = false;
		if(byteArray.length < startBytes.length){
			result = false;
		} else {
			for(int i = 0; i < startBytes.length; ++i) {
				if(byteArray[i] != startBytes[i]){
					result = false;
					break;
				}
			}
			result = true;
		}
		return result;
	}
	
	public static boolean byteArrayEndWith(byte[] byteArray, byte... endBytes) {
		boolean result = false;
		if(byteArray.length < endBytes.length){
			result = false;
		} else {
			for(int i = byteArray.length - endBytes.length; i < endBytes.length; ++i) {
				if(byteArray[i] != endBytes[i]){
					result = false;
					break;
				}
			}
			result = true;
		}
		return result;
	}
	
	public static byte[] byteArraySub(byte[] byteArray, int start, int length){
		byte[] result = new byte[length];
		System.arraycopy(byteArray, start, result, 0, length);
		return result;
	}
	
	public static byte[] byteArraySub(byte[] byteArray, int start){
		return byteArraySub(byteArray, start, byteArray.length - start);
	}
	
	public static byte[] byteArrayAppend(byte[] byteArray1, byte[] byteArray2){
		byte[] result = new byte[byteArray1.length + byteArray2.length];
//		for(int i = 0; i < byteArray1.length; ++i){
//			result[i] = byteArray1[i];
//		}
		System.arraycopy(byteArray1, 0, result, 0, byteArray1.length);
//		for(int i = byteArray1.length; i < result.length; ++i){
//			result[i] = byteArray2[i - byteArray1.length];
//		}
		System.arraycopy(byteArray2, 0, result, byteArray1.length, byteArray2.length);
		return result;
	}
	
	public static byte[] byteArrayPutHead(byte[] byteArray, byte[] head){
		byte[] result = new byte[byteArray.length + head.length];
		for(int i = 0; i < head.length; ++i){
			result[i] = head[i];
		}
		for(int i = head.length; i < result.length; ++i){
			result[i] = byteArray[i - head.length];
		}
		return result;
	}
	
	public static byte[] byteArrayPutTail(byte[] byteArray, byte[] tail){
		byte[] result = new byte[byteArray.length + tail.length];
		for(int i = 0; i < byteArray.length; ++i){
			result[i] = byteArray[i];
		}
		for(int i = byteArray.length; i < result.length; ++i){
			result[i] = tail[i - byteArray.length];
		}
		return result;
	}
	
	public static byte[] byteArrayGetHead(byte[] byteArray, int headNumber){
		byte[] result = new byte[headNumber];
		System.arraycopy(byteArray, 0, result, 0, headNumber);
		return result;
	}
	
	public static byte[] byteArrayGetTail(byte[] byteArray, int tailNumber){
		if(tailNumber >= byteArray.length){
			return byteArray;
		}
		if(tailNumber <= 0){
			return new byte[0];
		}
		byte[] result = new byte[tailNumber];
		for(int i = 0; i < tailNumber; ++i){
			result[i] = byteArray[byteArray.length - tailNumber + i];
		}
		return result;
	}
	
	public static byte[] byteArrayCutHead(byte[] byteArray, int headNumber){
		byte[] result = new byte[byteArray.length - headNumber];
		System.arraycopy(byteArray, headNumber, result, 0, result.length);
		return result;
	}
	
	public static byte[] byteArrayCutTail(byte[] byteArray, int tailNumber){
		if(tailNumber >= byteArray.length){
			return new byte[0];
		}
		if(tailNumber <= 0){
			return byteArray;
		}
		byte[] result = new byte[byteArray.length - tailNumber];
		for(int i = 0; i < result.length; ++i){
			result[i] = byteArray[i];
		}
		return result;
	}
	
	public static byte verify(byte[] byteArray){
		if(byteArray.length == 0){
			return 0x00;
		}
		byte b = byteArray[0];
		for(int i = 1; i < byteArray.length; ++i){
			b = (byte) (b ^ byteArray[i]);
		}
		return b;
	}
	
	public static byte verify(byte[] byteArray, int fromPos, int toPos){
		if(byteArray.length == 0){
			return 0x00;
		}
		byte b = byteArray[fromPos];
		for(int i = fromPos + 1; i <= toPos; ++i){
			b = (byte) (b ^ byteArray[i]);
		}
		return b;
	}
	
	public static byte[] byteArrayXor(byte[] byteArray1, byte[] byteArray2){
		byte[] result = new byte[byteArray1.length];
		for(int i = 0; i < result.length; ++i){
			result[i] = (byte) (byteArray1[i] ^ byteArray2[i]);
		}
		return result;
	}
	
	public static byte[] byteArrayAdd(byte[] byteArray1, byte[] byteArray2){
		if(byteArray1.length != byteArray2.length){
			return null;
		}
		byte[] result = new byte[byteArray1.length];
		for(int i = 0; i < result.length; ++i){
			result[i] = (byte) (byteArray1[i] + byteArray2[i]);
		}
		return result;
	}
	
//	public static List<byte[]> fetchList(byte[] byteArray, int payloadLength, byte head){
//		List<byte[]> tcerList = new ArrayList<byte[]>();
//		int p = 0; // the begin read position of the byte array
//		int c = 0; // the integral part count without residue
//		while (c < byteArray.length / (payloadLength - 5)) {
//			byte[] e = new byte[payloadLength];
//			e[0] = (byte) 0x9b;
//			e[1] = head;
//			e[2] = (byte) (c + 1);
//			e[3] = (byte) 0x00; // marked as whether the other side has received all bytes 
//			System.arraycopy(byteArray, p, e, 4, payloadLength - 5);
//			tcerList.add(e);
//			p += payloadLength - 5;
//			++c;
//		}
//		if(p != byteArray.length){
//			byte[] e = new byte[payloadLength];
//			e[0] = (byte) 0x9b;
//			e[1] = (byte) head;
//			e[2] = (byte) (c + 1);
//			e[3] = (byte) 0x00; // marked as whether the other side has received all bytes
//			System.arraycopy(byteArray, p, e, 4, byteArray.length - p);
//			tcerList.add(e);
//		}
//		return tcerList;
//	}
	
	public static List<byte[]> fetchList(byte[] byteArray, int payloadLength, byte head){
		List<byte[]> tcerList = new ArrayList<byte[]>();
		int p = 0; // the begin read position of the byte array
		int c = 0; // the integral part count without residue
		while (c < byteArray.length / (payloadLength - 4)) {
			byte[] e = new byte[payloadLength];
			e[0] = (byte) 0x9b;
			e[1] = head;
			e[2] = (byte) (c + 1);
			System.arraycopy(byteArray, p, e, 3, payloadLength - 4);
			e[payloadLength - 1] = ByteUtil.verify(e, 0, payloadLength - 2);
			tcerList.add(e);
			p += payloadLength - 4;
			++c;
		}
		if(p != byteArray.length){
			byte[] e = new byte[payloadLength];
			e[0] = (byte) 0x9b;
			e[1] = (byte) head;
			e[2] = (byte) (c + 1);
			System.arraycopy(byteArray, p, e, 3, byteArray.length - p);
			e[byteArray.length - p + 3] = ByteUtil.verify(e, 0, e.length - 2);
			tcerList.add(e);
		}
		return tcerList;
	}
	
//	public static List<byte[]> fetchList(byte[] byteArray, int payloadLength, byte head, int persize){
//		List<byte[]> tcerList = new ArrayList<byte[]>();
//		int p = 0; // the begin read position of the byte array
//		int c = 0; // the integral part count without residue
//		for(c = 0; c < byteArray.length / persize; ++c){
//			byte[] e = new byte[payloadLength];
//			e[0] = (byte) 0x9b;
//			e[1] = head;
//			e[2] = (byte) c;
//			System.arraycopy(byteArray, p, e, Constants.HEAD_SIZE, persize);
//			tcerList.add(e);
//			p += persize;
//		}
//		if(p != byteArray.length){
//			byte[] e = new byte[payloadLength];
//			e[0] = (byte) 0x9b;
//			e[1] = (byte) head;
//			e[2] = (byte) c;
//			System.arraycopy(byteArray, p, e, Constants.HEAD_SIZE, byteArray.length - p);
//			tcerList.add(e);
//		}
//		return tcerList;
//	}
	
	/**
	 * set a bit to one
	 * @param src a number
	 * @param position bit position count from right
	 * @return
	 */
	public static int setBitOne(int src, int position){
		return (src | (1 << position));
	}
	
	/**
	 * make a number that all bits are one
	 * @param digits how many digits the number has
	 * @return
	 */
	public static int makeAllOneBits(int digits){
		return ((1 << digits) - 1);
	}
	
	/**
	 * judge that if all low bits of a number be one
	 * @param src the number
	 * @param digits the low bit digits number count from right
	 * @return
	 */
	public static boolean isAllBitOne(int src, int digits){
		return ((src & makeAllOneBits(digits)) == makeAllOneBits(digits));
	}
	
	/**
	 * judge that if the position bit of the number is one
	 * @param src the number
	 * @param position the bit of the number count from right
	 * @return
	 */
	public static boolean isThisBitOne(int src, int position){
		return ((src & (1 << position)) == (1 << position));
	}
	
	public static byte[] fetchContent(byte[] buffer){
		for(int i = 0; i < buffer.length; ++i){
			if(buffer[i] == 0x00){
				return byteArraySub(buffer, 0, i);
			}
		}
		return buffer;
	}
	
	public static byte[] newByteArray(int size, byte initByte){
		byte[] result = new byte[size];
		for(int i = 0; i < size; ++i){
			result[i] = initByte;
		}
		return result;
	}
	
//	public static byte[] stringToByte(String s){
//		byte[] result = new byte[]{};
//		try {
//			result = s.getBytes("Unicode");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//	
//	public static String byteToString(byte[] byteArray){
//		String s = "";
//		try {
//			s = new String(byteArray, "Unicode");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		return s;
//	}
	
	public static byte[] stringToByte(String s){
		byte[] result = new byte[]{};
		try {
			result = s.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static byte[] stringToGBKByte(String s){
		byte[] result = new byte[]{};
		try{
			result = s.getBytes("gbk");
		} catch (UnsupportedEncodingException e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String byteToString(byte[] byteArray){
		String s = "";
		try {
			s = new String(byteArray, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static String gbkByteToString(byte[] byteArray){
		String s = "";
		try {
			s = new String(byteArray, "gbk");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static byte[] fetchRandom(int size){
		byte[] result = new byte[size];
		Random random = new Random(System.currentTimeMillis());
		random.nextBytes(result);
		return result;
	}

	public static byte[] removeBytes(byte[] byteArray, byte byteToRemove){
		List<Byte> resultList = new LinkedList<Byte>();
		for(int i = 0; i < byteArray.length; ++i){
			byte b = byteArray[i];
			if(b != byteToRemove){
				resultList.add(byteArray[i]);
			}
		}
		byte[] result = new byte[resultList.size()];
		for(int i = 0; i < resultList.size(); ++i){
			result[i] = resultList.get(i);
		}
		return result;
	}
	
}
